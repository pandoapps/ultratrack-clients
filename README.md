# README #

#Configuração de Ambiente do Projeto

Instalar o Postgres e PostGIS, e executar no postgres:
CREATE EXTENSION postgis

#Google API Key

Obter uma api key em: https://developers.google.com/maps/documentation/javascript/get-api-key

#Adicionar ao .env file

Variáveis a serem adicionadas ao .env devem ser exatamente conforme abaixo descrito:

GOOGLE_API_KEY= api-key-gerada-pelo-google. Exemplo: GOOGLE_MAP_KEY=AIzaSyD13udfh61Pt-YecqPtDmh_KqJyeFZi8yA
FIELD_KEY=chave de segurança da empresa fornecida pelo UltraTrack. Exemplo: FIELD_KEY=he1827eh1827ehr24gyhng3
ADDRESS_ZMQ=porta utilizada pelo zmq. Exemplo: ADDRESS_ZMQ=127.0.0.1:4848
ADDRESS_WEBSOCKET=porta do pusher. Exemplo: ADDRESS_WEBSOCKET=127.0.0.1:9898

#Websockets

Instalar o pacote do php ZMQ (por exemplo sudo apt-get install php7.0-zmq)

Liberar portas Para funcionar o websocket precisa ter liberado as portas: 5555 e 8080

Executar: cd bin   
E depois: php push-server.php

#Upload de imagens

Fazer link simbólico de public/storage para storage/app/public rodando o comando: php artisan storage:link

#Primeiro login da empresa, após o primeiro SYNC

A senha do primeiro login é 123456

#Outras considerações

Durate testes: Lembrar de atualizar as datas do seed de listens e event_actions, pois, o mapa por default só exibe os dados em um intervalo de um mês a partir da data de hoje.

As listens e e event actions são exibidas no mapa de acordo com seu campo listened_at.

Os seeds são apenas para teste. Após o término do teste é necessário apagar os dados inseridos para que se faça o sync. O único seed que deve ser obrigatoriamente executado é o RoleTableSeeder.

O modelo relacional do banco de dados está na pasta Downloads do Bitbucket.

O timestamp recebido do PandaPost pela api listen é 1000 vezes maior, por isso divide-se ela por 1000. Caso essa questão mude, é necessário tirar a divisão por 1000 da ListenController.php e home.blade.php .
