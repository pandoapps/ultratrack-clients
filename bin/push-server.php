<?php

    require dirname(__DIR__) . '/vendor/autoload.php';
    use Ratchet\Server\IoServer;
    use Ratchet\Http\HttpServer;
    use Ratchet\WebSocket\WsServer;
    use React\EventLoop\Factory;
    use React\ZMQ\Context;
    use App\Websockets\Pusher;

    $loop   = Factory::create();
    $pusher = new App\Websockets\Pusher;

    // Listen for the web server to make a ZeroMQ push after an ajax request

    //recupero do env o endereço zmq e websocket
    $path = "../";
    $dotenv = new \Dotenv\Dotenv($path);
    $dotenv->load();
    $address_zmq = getenv('ADDRESS_ZMQ');
    $port_websocket = getenv('ADDRESS_WEBSOCKET');
    $port_websocket = explode(":", $port_websocket)[1]; //pega apenas os caracteres que estão depois do ":" no address websocket

    $context = new Context($loop);
    $pull = $context->getSocket(ZMQ::SOCKET_PULL);
    $pull->bind('tcp://'.$address_zmq); // Binding to 127.0.0.1 means the only client that can connect is itself
    $pull->on('open', array($pusher, 'onSubscribe'));
    $pull->on('message', array($pusher, 'onListenEntry'));

    // Set up our WebSocket server for clients wanting real-time updates
    $webSock = new React\Socket\Server('0.0.0.0:'.$port_websocket, $loop); // Binding to 0.0.0.0 means remotes can connect
    $webServer = new Ratchet\Server\IoServer(
        new Ratchet\Http\HttpServer(
            new Ratchet\WebSocket\WsServer(
                new Ratchet\Wamp\WampServer(
                    $pusher
                )
            )
        ),
        $webSock
    );

    $loop->run();