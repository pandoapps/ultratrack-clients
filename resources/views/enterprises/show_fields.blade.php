<!-- Picture Field -->
<div class="form-group">
    {!! Form::label('Logo', ' ') !!}    
    <!-- Verifica se a foto está armazenada em um site, pois quando faz o sync as imagens vão estar armazenadas no site da UltraTrack -->
    <!-- h de http -->
    @if($enterprise->logo[0] == 'h') 
        <img src="{{ $enterprise->logo }}" height="120px" width="100px"/>
    @else
        <img src="{{ '/storage/'.$enterprise->logo }}" height="120px" width="100px"/>
    @endif
</div>

<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $enterprise->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Nome:') !!}
    <p>{!! $enterprise->name !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $enterprise->email !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Telefone:') !!}
    <p>{!! $enterprise->phone !!}</p>
</div>

<!-- Document Field -->
<div class="form-group">
    {!! Form::label('document', 'Documento:') !!}
    <p>{!! $enterprise->document !!}</p>
</div>

<!-- Field Key Field -->
<div class="form-group">
    {!! Form::label('field_key', 'Chave:') !!}
    <p>{!! $enterprise->field_key !!}</p>
</div>

<!-- Endpoint Field -->
<div class="form-group">
    {!! Form::label('endpoint', 'Endpoint:') !!}
    <p>{!! $enterprise->endpoint !!}</p>
</div>

<!-- Status Enterprise Id Field -->
<div class="form-group">
    {!! Form::label('status_enterprise_id', 'Status Enterprise Id:') !!}
    <p>{!! $enterprise->status_enterprise_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Criado em:') !!}
    <p>{!! $enterprise->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Atualizado em:') !!}
    <p>{!! $enterprise->updated_at !!}</p>
</div>

