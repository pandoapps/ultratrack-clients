<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Is New Field -->
<div class="form-group col-sm-12">
    {!! Form::label('is_new', 'Is New:') !!}
    <label class="radio-inline">
        {!! Form::radio('is_new', "1", null) !!} Sim
    </label>

    <label class="radio-inline">
        {!! Form::radio('is_new', "0", null) !!} Nao
    </label>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('messages.index') !!}" class="btn btn-default">Cancel</a>
</div>
