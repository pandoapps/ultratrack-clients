<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $message->id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $message->title !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $message->description !!}</p>
</div>

<!-- Is New Field -->
<div class="form-group">
    {!! Form::label('is_new', 'Is New:') !!}
    <p>{!! $message->is_new !!}</p>
</div>

<!-- Enterprise Id Field -->
<div class="form-group">
    {!! Form::label('enterprise_id', 'Enterprise Id:') !!}
    <p>{!! $message->enterprise_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $message->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $message->updated_at !!}</p>
</div>

