<script>

var travelMode, contador;

function geraRelatorio(){   
    timeoutButton();

    //verifica que tipo de rota o usuário deixou marcado para traçar    
    var routeType = $('input[name="radio-route"]:checked').val();
    
    if (routeType == "walk"){
        travelMode = google.maps.TravelMode.WALKING;
    }
    else{ //se for do tipo line ou driving
        travelMode = google.maps.TravelMode.DRIVING;
    }

    @foreach($tags as $tag)
        //busca todas as listens daquela tag
        contador=0; //vai servir para se descobrir quanto tempo a pessoa ficou parada em horas
        $.ajax({
            type: "GET",
            url: "{{route('tag_tracker')}}",
            dataType: 'json',
            //Parâmetros da call: tag_id, start e end daterangepicker
            data: {tag_id:{{$tag->id}}, start_date: startDate, end_date: endDate},
            success: function(data) {                
                var i=0;
                while (i < data.items.length-1){ //vai até data.items.length-1, pois é considerado o elemento i e o i+1
                    way_pts1 = data.items[i];
                    listen1 = new google.maps.LatLng(way_pts1[0], way_pts1[1]);

                    way_pts2 = data.items[i+1];
                    listen2 = new google.maps.LatLng(way_pts2[0], way_pts2[1]);

                    i++;

                    calcularDistancia(listen1, listen2);
                }
            }
        });
    @endforeach
    //alert("Relatório\n Tag");
}

function calcularDistancia(listen1, listen2) { //tem que ser chamado em um for
    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix({
        origins: [listen1],
        destinations: [listen2],
        travelMode: travelMode,
    }, callback);  
}

function callback(response, status) {
    //Verifica o Status
    if (status != google.maps.DistanceMatrixStatus.OK)
        Materialize.toast('Não foi possível construir o relatório corretamente.', 4200);
    else {
        distancia = response.rows[0].elements[0].distance.text;
        unidade_medida = distancia.split(" ")[1];
        distancia = distancia.split(" ")[0];

        console.log(distancia);
        console.log(unidade_medida);

        if((distancia <= 5) && (unidade_medida == "m")){
            contador++;
        }
        console.log(contador); // erro: por algum motivo o contador não está zerando quando muda de tag

        //response.rows[0].elements[0].duration.text; //duração do percurso entre os dois pontos
    }
}
</script>