<script>

var conn = new ab.Session('ws://{{env('ADDRESS_WEBSOCKET')}}',
    function() {
        @foreach($tags as $tag) //inscrição em cada tag que lhe pertence       
            //usuário se inscreve para receber atualizações das tags que lhe pertence
            conn.subscribe('{{$tag->id}}', function(topic, data) {

                //Atualiza as variáveis de data para pesquisa
                $('#daterange').val(startDate + ' - ' + endDate);

                if (data.timestamp==-999){
                    bounds = new google.maps.LatLngBounds();
                    @foreach($tags as $key3=>$tag)
                        if (data.tag.id == {{$tag->id}}){
                            var marker = markers_array[{{$key3}}];//-1 pois inicia com 0
                            var marker_index = {{$key3}};
                            var new_position = new google.maps.LatLng(parseFloat(data.lat), parseFloat(data.lng));
                            marker.setPosition(new_position);
                            if(markers_array[{{$key3}}].getVisible()) { //verifica se o checkbox dessa tag está marcado
                                if (directionsDisplayArray[{{$key3}}] || polyline_array[{{$key3}}]){ //rotas estavam traçadas 
                                    calcRoute(); 
                                }
                                else{
                                    bounds.extend(new_position);
                                    map.fitBounds(bounds);
                                }
                            }                              
                        }
                        else{
                            if(markers_array[{{$key3}}].getVisible()) { //verifica se o checkbox dessa tag está marcado
                                var position = new google.maps.LatLng({{$tag->getCoord()[0]}}, {{$tag->getCoord()[1]}});
                                bounds.extend(position);
                                map.fitBounds(bounds);
                            }
                        }                    
                    @endforeach 
                }
                else{
                    @foreach($tags as $tag)
                        if (data.tag.id == {{$tag->id}}){  
                            addEventActions(data.lat, data.lng, data.tag.name, data.timestamp,data.tag.id);
                        }                           
                    @endforeach                     
                }            
            });
        @endforeach
    },
    function() {
        console.warn('WebSocket connection closed');
    },
    {'skipSubprotocolCheck': true}
);

</script>