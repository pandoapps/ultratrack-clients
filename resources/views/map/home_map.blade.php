<script>


/***** CHECKBOXs *****/

// Marca todos checkboxes ao recarregar a página
$( document ).ready( function(){
    var checkboxes = $( ':checkbox' );
    
    checkboxes.prop( 'checked', true );

});

// Fazendo com que ao clicar na div do checkbox já o marque/desmarque
$('.marker-div').click(function () {

 if ($(this).find('input:checkbox').is(":checked")) {
     $(this).find('input:checkbox').attr("checked", false);
 } else {
     $(this).find('input:checkbox').attr("checked", true);
 }

});

$('input[type=checkbox]').click(function (e) {
  e.stopPropagation();
});

/***** FIM - CHECKBOXs *****/


/***** DATERANGEPICKER - Calendário *****/

$('#daterange').daterangepicker({
    alwaysShowCalendars: true,
    timePicker: true,
    timePicker24Hour: true,
    timePickerSeconds: true,
    autoApply: true,
    autoUpdateInput: true,
    autoUpdateInput: true,
    locale: {
        'format': 'DD/MM/YYYY HH:mm:ss',
        'separator': ' - ',
        'applyLabel': 'Aplicar',
        'cancelLabel': 'Cancelar',
        'fromLabel': 'De',
        'toLabel': 'Para',
        'customRangeLabel': 'Customizar',
        'daysOfWeek': ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb'],
        'monthNames': ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
    },
    ranges: {
        'Última hora': [moment().subtract(1, 'hours'), moment()],
        'Últimas 6 horas': [moment().subtract(6, 'hours'), moment()],
        'Último dia': [moment().subtract(1, 'days'), moment()],
        'Última semana': [moment().subtract(6, 'days'), moment()]
    },
    startDate: moment().subtract(29, 'days'),
    endDate: moment(),
    maxDate: moment(),
    opens: 'center',
    drops: 'up'
});

// Inicializa variáveis que vão conter as datas para passar como parâmetro por AJAX para consulta das Listens
var startDate = $('#daterange').val().split('-')[0];
var endDate = $('#daterange').val().split('-')[1];

$('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
   $(this).val(picker.startDate.format('DD/MM/YYYY HH:mm:ss') + ' - ' + picker.endDate.format('DD/MM/YYYY HH:mm:ss'));
   //Atualiza as variáveis de data com o conteúdo do datetimepicker ao atualizar
   startDate =   picker.startDate.format('YYYY-MM-DD HH:mm:ss');
   endDate   =   picker.endDate.format('YYYY-MM-DD HH:mm:ss');
});
$('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
   $(this).val('');
   startDate = undefined;
   endDate   = undefined;
});

/***** FIM - DATERANGEPICKER *****/


/***** CREATE MAP *****/

//declaração dos arrays de localização de tags e de event_actions
var locations_tags = [
    @foreach($tags as $tag)
        ['{{$tag->name}}', {{$tag->getCoord()[0]}}, {{$tag->getCoord()[1]}}, '{{$tag->updated_at}}'],
    @endforeach
];

var locations_event_actions = [
   @foreach($event_actions as $event_action)
       [{{$event_action->getCoord()[0]}}, {{$event_action->getCoord()[1]}}, 
       '{{$event_action->listened_at}}', '{{$event_action->name}}','{{$event_action->tag_id}}'],
   @endforeach
];

//Cria o Mapa
if(locations_tags.length>0){ //o usuário possui ao menos uma tag

   var styles_map = map_style_with_tag(); //estilo definido em js/map_style.js
   var map = new google.maps.Map(document.getElementById('map-canvas'), {
       zoom: 10,
       center: new google.maps.LatLng(locations_tags[0][1], locations_tags[0][2]),
       mapTypeId: google.maps.MapTypeId.ROADMAP,
       styles: styles_map
   });
}
else { //o usuário possui nenhuma tag

   var styles_map = map_style_without_tag(); //estilo definido em js/map_style.js
   var map = new google.maps.Map(document.getElementById('map-canvas'), {
       zoom: 10,
       center: new google.maps.LatLng('-22.4253360', '-45.4528560'),
       mapTypeId: google.maps.MapTypeId.ROADMAP,
       styles: styles_map,
       disableDefaultUI: true,
   });
   $('#routebtn').attr('disabled', 'disabled');
   $('#daterange').attr('disabled', 'disabled');

   Materialize.toast('Você ainda não possui nenhuma tag. Entre em contato com a empresa para receber mais informações.', 5500);

}

/***** FIM - CREATE MAP *****/


/***** MARKERS *****/

var geocoder = new google.maps.Geocoder;
var infowindow = new google.maps.InfoWindow();
var i;
var markers_array = [];
var markers = [];
var polyline_array = [];
var bounds = new google.maps.LatLngBounds();

//insere marcadores da última localização das tags no mapa
pointMarkersOnMap();

function pointMarkersOnMap(){
   for (i = 0; i < locations_tags.length; i++) {
   
       var marker;
       //Cria Marcadores nos Pontos das Tags
       marker = new google.maps.Marker({
           position: new google.maps.LatLng(locations_tags[i][1], locations_tags[i][2]),
           map: map,
           zIndex: 999 
       });

       bounds.extend(marker.position);
       markers_array[i]=marker;

       //Evento de clicar no Marcador (tag)
       google.maps.event.addListener(marker, 'click', (function(marker, i) {
           return function() {
               geocoder.geocode({'location': marker.getPosition()}, function(results, status) {
                   if (status === 'OK') {
                       if (results[1]) {
                           map.setCenter(marker.getPosition());
                           
                           var date = new Date(locations_tags[i][3]); // converte para data
                           // Configura o conteudo da janela do Marcador
                           infowindow.setContent(
                               "<strong>"+locations_tags[i][0]+"</strong>"+"<br><br>"+
                               results[0].formatted_address+"<br>"+
                               "("+marker.getPosition().lat()+", "+marker.getPosition().lng()+")"+"<br><br>"+
                               "Última atualização: "+date.toLocaleDateString("pt-BR") +" "+
                               + date.getHours() +":"+ date.getMinutes() +":"+ date.getSeconds()
                           );
                           // Abre a janela
                           infowindow.open(map, marker);
                       } else {
                            Materialize.toast('Nenhum resultado encontrado', 3500);
                       }
                   } else {
                        Materialize.toast('Geocoder falhou devido a: ' + status +' .Local desconhecido.', 4000);
                   }
               });
           }
       })(marker, i));
   }
   // Enquadra a tela nos bounds(lat e lng dos marcadores)
   map.fitBounds(bounds);
}

var end_date, start_date;

function formatData(){
    //o startDate possui um espaço em branco no início da string devido ao " - " que o separa do endDate, e isso teve que ser tratado
    day = endDate.split("/")[0].split(" ")[1];
    month = endDate.split("/")[1];
    year = endDate.split("/")[2].split(" ")[0];
    hours =  endDate.split(" ")[2].split(":")[0];
    minutes = endDate.split(" ")[2].split(":")[1];
    seconds = endDate.split(" ")[2].split(":")[2];
    end_date = year+"-"+month+"-"+day+" "+hours+":"+minutes+":"+seconds;

    day = startDate.split("/")[0];
    month = startDate.split("/")[1];
    year = startDate.split("/")[2].split(" ")[0];
    hours =  startDate.split(" ")[1].split(":")[0];
    minutes = startDate.split(" ")[1].split(":")[1];
    seconds = startDate.split(" ")[1].split(":")[2];
    start_date = year+"-"+month+"-"+day+" "+hours+":"+minutes+":"+seconds;

    return;
}

var markers_event_actions_array = [];
var cont = 0;

//insere marcadores das event_actions das tags no mapa
pointMarkersEventActionsOnMap();

function pointMarkersEventActionsOnMap(){
    
   formatData();

   for (i = 0; i < locations_event_actions.length; i++) {    
        //event_action registrada no intervalo determinado pelo usuário no calendário
        if ((locations_event_actions[i][2] >= start_date) && (locations_event_actions[i][2] <= end_date)){
            var marker;
            cont = i; 
            //Cria Marcadores
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations_event_actions[i][0], locations_event_actions[i][1]),
                map: map,
                title: locations_event_actions[i][4]+" - ID da tag "+locations_event_actions[i][3],
                icon:'/Home_files/warning_maker.png'
            });

            bounds.extend(marker.position);
            markers_event_actions_array[i]=marker;

            // Evento de clicar no marcador (tag)
            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    var latlng = {lat: parseFloat(locations_event_actions[i][0]), lng: parseFloat(locations_event_actions[i][1])};
                    geocoder.geocode({'location': latlng}, function(results, status) {
                        if (status === 'OK') {
                            if (results[1]) {
                                map.setCenter(latlng);
                                var date = new Date(locations_event_actions[i][2]); // converte para data
                                // Configura o conteudo da janela do Marcador
                                infowindow.setContent(
                                    "<strong>Acionado o botão de ação da tag "+locations_event_actions[i][3]+"</strong>"+"<br><br>"+
                                    results[0].formatted_address+"<br>"+
                                    "("+latlng.lat+", "+latlng.lng+")"+"<br><br>"+
                                    "Registrado em: "+date.toLocaleDateString("pt-BR") +" "+
                                    + date.getHours() +":"+ date.getMinutes() +":"+ date.getSeconds()
                                );
                                // Abre a janela
                                infowindow.open(map, marker);
                            } else {
                                Materialize.toast('Nenhum resultado encontrado', 3500);
                            }
                        } else {
                            Materialize.toast('Geocoder falhou devido a: ' + status +' .Local desconhecido.', 4000);
                        }
                    });
                }
            })(marker, i));
        }
    }
    map.fitBounds(bounds);
}

/***** FIM - MARKERS *****/


/***** OPERATIONS - CHECKBOXs *****/

//Para não dar erro se o usuário ficar marcando e desmarcando várias vezes seguidas o checkbox
function timeoutCheckbox(id){
    $('#'+id).attr("disabled", true);

    setTimeout(function(){ 
        $('#'+id).attr("disabled", false);
    }, 1100);
}

// Checkbox da Tag 'checked' -> Marcador fica Visível || Checkbox false -> Marcador invisível
function toggleGroup(k, tag_id) {
     var marker = markers_array[k];
     var title;
     
     timeoutCheckbox(k);

     if (!marker.getVisible()) {
         marker.setAnimation(google.maps.Animation.DROP);
         marker.setVisible(true);

        for (i=0; i<markers_event_actions_array.length; i++) {
            title = markers_event_actions_array[i].getTitle();
            title = title.split(" ")[0];    
            if(title == tag_id)
                markers_event_actions_array[i].setVisible(true);
        }

     } else {
         marker.setVisible(false);
         if (directionsDisplayArray[k])
            cleanRoutesByDisplay(k);
         else if (polyline_array[k])
            cleanPolylines(k);

        for (i=0; i<markers_event_actions_array.length; i++) {
            title = markers_event_actions_array[i].getTitle();
            title = title.split(" ")[0];  
            if(title == tag_id)
                markers_event_actions_array[i].setVisible(false);
        }
     }
     fitBoundsToVisibleMarkers(k);     
}

//Redimensiona o mapa para exibir as localizações das tags que estão selecionadas
function fitBoundsToVisibleMarkers(k) {
    bounds = new google.maps.LatLngBounds();
    var i;
    for (i=0; i<markers_array.length; i++) {
        if(markers_array[i].getVisible()) {
            if ((directionsDisplayArray[i] || polyline_array[i]) && (i!=k)){ //rotas já estavam traçadas 
                calcRoute();
            }
            else if (!directionsDisplayArray[i] && !polyline_array[i]){
                bounds.extend(markers_array[i].getPosition()); 
            }            
        }
    }
    map.fitBounds(bounds);
}

/***** FIM - OPERATIONS - CHECKBOXs *****/


/***** CLEAR - MAP *****/

function cleanRoutesByDisplay(tag_key){
    if(directionsDisplayArray[tag_key]){
        directionsDisplayArray[tag_key].setDirections({routes: []});
        directionsDisplayArray[tag_key].setMap(null);
    }
}

function cleanPolylines(tag_key) {
    if(polyline_array[tag_key]){
        polyline_array[tag_key].setMap(null);
        markers[tag_key].setMap(null);
    }
}

function cleanAllRoutesByDisplay(){
    var i;
    for (i=0;i<directionsDisplayArray.length;i++){
        if (directionsDisplayArray[i]){
            directionsDisplayArray[i].setDirections({routes: []});
            directionsDisplayArray[i].setMap(null);
        }
    }
}

function cleanAllPolylines() {
    polyline_array.forEach(function(polyline){
        polyline.setMap(null);
    });
    var i;
    for (i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }

    markers = [];
}

/***** FIM - CLEAR - MAP *****/


/***** ROUTES *****/

var directionsService = new google.maps.DirectionsService();
var directionsDisplayArray = [];

// Evento de clicar no botão 'Traçar Rota'
google.maps.event.addDomListener(document.getElementById('routebtn'), 'click', calcRoute);

// Evento de clicar no botão 'Gerar Relatório'
google.maps.event.addDomListener(document.getElementById('relatoriobtn'), 'click', geraRelatorio);

// pra não dar erro se o usuário ficar apertando várias vezes seguidas o botão de traçar rotas e exibir relatório
function timeoutButton(){
    $('#routebtn').attr("disabled", true);
    $('#relatoriobtn').attr("disabled", true);

    setTimeout(function(){ 
        $('#routebtn').attr("disabled", false);
    }, 1100);

    setTimeout(function(){ 
        $('#relatoriobtn').attr("disabled", false);
    }, 1100);
}

function randomFunc(inferior,superior){
  numPossibilidades = superior - inferior;
  aleat = Math.random() * numPossibilidades;
  aleat = Math.floor(aleat);
  return parseInt(inferior) + aleat;
} 

//gera cores aleatórias para as rotas
function randomColorGiving(){
  hexadecimal = new Array("0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F");
  cor_aleatoria = "#";
  for (i=0;i<6;i++){
     posarray = randomFunc(0,hexadecimal.length);
     cor_aleatoria += hexadecimal[posarray];
  }
  return cor_aleatoria;
} 

function calcRoute() {
    
    timeoutButton();
    var inputs = Array.from($("input:checkbox:checked"));
    // drawType = drive, walk, ou line
    var drawType = $('input[name="radio-route"]:checked').val();

    // Limpa todas as rotas existentes antes de gerá-las
    cleanAllRoutesByDisplay();
    // Limpa as rotas de linha reta (polylines)
    cleanAllPolylines();

    // Inicio do foreach - Para cada Tag do usuário
    //key = índice do array de tags
    @foreach($tags as $key2=>$tag)
        var key2 = {{$key2}};

        //Para cada Tag com checkbox marcado
        inputs.forEach(function(checked) {
            var tag_key = parseInt(checked.id);
            //Se tal Tag está com o checkbox marcado 
            if (tag_key == {{$key2}}){
                //AJAX consulta todos os Listens desta Tag
                $.ajax({
                    type: "GET",
                    url: "{{route('tag_tracker')}}",
                    dataType: 'json',
                    //Parâmetros da call: tag_id, start e end daterangepicker
                    data: {tag_id:{{$tag->id}}, start_date: startDate, end_date: endDate},
                    success: function(data) {
                        //Se existem pelo menos 2 Listens no período de tempo escolhido
                        var num_items = data.items.length;
                        if (num_items >= 2){
                            if (drawType == "line"){
                                drawStraightLine(tag_key, data.items);
                            }else{
                                if(num_items > 23){
                                    // Descomentar os "consoles.log" para debuggar e entender melhor.
                                    // console.log("Número total de itens: "+num_items);

                                    // Array com os nodes
                                    var nodes=[]
                                    nodes[0] = [];
                                    nodes[0].push(divHandler(num_items/2));
                                
                                    for(i=1;i<5;i++){
                                        nodes[i]=[];
                                        var j = 0;
                                        nodes[i-1].forEach(function(node){
                                            if(i!=4){
                                                // No filho a esquerda, na lógica da árvore
                                                var left_child = node - divHandler(nodes[i-1][0]/2);
                                                nodes[i].push(left_child);

                                                // No filho a direita, na lógica da árvore
                                                var right_child = node + divHandler(nodes[i-1][0]/2);
                                                nodes[i].push(right_child);
                                                //console.log("node :"+node+ ", has left son: "+left_child+", and right son: "+right_child);
                                            }else{
                                                var node2 = [];
                                                for(var x=0;x<nodes[2].length;x++){
                                                    node2.push(nodes[2][x]);
                                                    node2.push(nodes[2][x]);
                                                }
                                                var only_child = divHandler((node+node2[j])/2);
                                                //console.log("node + nodes[i-2][j])/2 = "+node+"+"+nodes[i-2][j]+"/2");
                                                nodes[i].push(only_child);
                                                j++;
                                            }
                                        });
                                        //console.log("Finished i="+i+", with nodes[i]="+nodes[i]);
                                    }
                                    /*
                                    console.log("Array de nodes0: "+nodes[0]);
                                    console.log("-------------------------");
                                    console.log("Array de nodes1: "+nodes[1]);
                                    console.log("-------------------------");
                                    console.log("Array de nodes2: "+nodes[2]);
                                    console.log("-------------------------");
                                    console.log("Array de nodes3: "+nodes[3]);
                                    console.log("-------------------------");
                                    console.log("Array de nodes4: "+nodes[4]);
                                    console.log("-------------------------");
                                    */

                                    // Novo Array NewWayPoints com os índices dos pontos intermediários que devem ser pegos
                                    // Com indices de 1 a 23, e valores de 1 até o número total de itens (data.items.lenght)
                                    
                                    // Árvore de nodes vira um array com os índices desordenados
                                    var newWayPoints = nodes.join().split(',').map(function(n) {return Number(n);});
                                    // Ordenação dos índices
                                    newWayPoints = newWayPoints.sort(function(a, b){return a - b});
                                    
                                    //newWayPoints[0]=1;
                                    //newWayPoints[22]=num_items;

                                    // O listens2 vai guardar os valores de LAT e LNG de cada índice selecionado
                                    var listens2 = [];
                                    for(i=0;i<23;i++){
                                        listens2[i] = data.items[newWayPoints[i]];
                                    }
                                    //console.log("Array de nodesTotal: "+newWayPoints);

                                    // São fixados o primeiro e o último índice como os valores de partida e chegada da rota
                                    listens2[0] = data.items[0];
                                    listens2[22] = data.items[num_items-1];
                                    //console.log("Array de nodesTotal: "+listens2);

                                    drawRoute(key2, tag_key, listens2, drawType);
                                } else{
                                    // (2 <= num_items <= 23)
                                    drawRoute(key2, tag_key, data.items, drawType);
                                }
                            }
                        } else{
                            //Existem menos de 2 Listens no período de tempo selecionado
                            Materialize.toast('Não existem leituras suficientes neste intervalo de tempo para gerar rotas para a Tag {{$tag->name}} .', 6000); //6000 é a duração da exibição da msg
                        }                     
                    }
                });
            }
        });
    @endforeach

    map.fitBounds(bounds);
}

// Division Handler: pega a divisão exata do número ou, caso seja decimal, pega o número par mais próximo pra cima ou pra baixo
function divHandler(number){
    n = Math.floor(number) % 2 == 0 ? Math.floor(number) : Math.ceil(number);
    if (n==0){
        n=1;
    }
    return n;
}

function drawStraightLine(tag_key, items){
    poly = new google.maps.Polyline({
        strokeColor: randomColorGiving(),
        strokeOpacity: 1.0,
        strokeWeight: 3
    });
    polyline_array[tag_key] = poly;
    poly.setMap(map);
    var path = poly.getPath();
    var cont_listens = 0;
    items.forEach(function(listen){
        var listen_latlng = new google.maps.LatLng(listen[0], listen[1])
        path.push(listen_latlng);

        if(cont_listens==0){ //exibe marker da primeira localização da rota, a primeira já vai estar marcada como last_point
            var marker_linear = new google.maps.Marker({
                position: listen_latlng,
                map: map
            });
            markers.push(marker_linear);
        }
        cont_listens++;

        if(cont_listens==items.length){
            cont_listens = 0;
        }

        bounds.extend(listen_latlng);
    });

    map.fitBounds(bounds);    
}

function drawRoute(key2, tag_key, items, drawType){
    //Define a primeira e a última listen como pontos de início e saída
    var way_pts = items;
    var location_start = way_pts.shift();
    var start = new google.maps.LatLng(location_start[0], location_start[1]);
    bounds.extend(start);
    var locations_end = way_pts.pop();
    var end = new google.maps.LatLng(locations_end[0], locations_end[1]);
    bounds.extend(end);

    //Transforma os pontos intermediários em locations waypoints (objetos)
    var way_pts_objs = [];
    way_pts.forEach(function(way){
        way_pts_objs.push({location: new google.maps.LatLng(way[0],way[1]),stopover: true})
        bounds.extend(new google.maps.LatLng(way[0],way[1]));
    });

    if (drawType == 'walk'){
        var travelMode = google.maps.TravelMode.WALKING;
    }
    else{
        var travelMode = google.maps.TravelMode.DRIVING;
    }

    map.fitBounds(bounds); 
    
    //Request do roteamento
    var request = {
        origin: start,
        destination: end,
        travelMode: travelMode
    }; //pode ser walking, bicycling ou driving
    
    //Se existir waypoints, adiciona sua location no request
    if (way_pts_objs){
        request.waypoints = way_pts_objs;
    }

    //Display das Rotas
    requestDirections(request, start, end, tag_key); 
}

function requestDirections(request, start, end, tag_key){
    directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            renderDirections(response, tag_key);
        } else {
            Materialize.toast('Não foi possível traçar rota entre ' + start.toUrlValue(6) + ' e ' + end.toUrlValue(6) + ': ' + status + '. Com ' + request.waypoints.length + ' waypoint(s).', 5500);
        }
    });
}

function renderDirections(response, tag_key) {
    //Cor das rotas (cada tag possui uma rota com cor diferente)
    directionsDisplayArray[tag_key] = new google.maps.DirectionsRenderer({
            preserveViewport: true 
    });

    var route_config = $('input[name="radio-route-config"]:checked').val();
    var type;
    
    if(route_config == 'show'){
        type = false;
    }
    else{
        type = true;
    }

    directionsDisplayArray[tag_key].setOptions({
        polylineOptions: {
            strokeColor: randomColorGiving(),
            strokeOpacity: 0.7,
        },
        suppressMarkers: type
    });
    directionsDisplayArray[tag_key].setDirections(response);
    directionsDisplayArray[tag_key].setMap(map);
}

/***** FIM - ROUTES *****/


/***** CREATE EVENT_ACTION APÓS RECEBER POST *****/

var lat_event_action, lng_event_action, timestamp_event_action, tag_name_event_action;

function addEventActions(lat, lng, tag_name, timestamp, tag_id){    
    timestamp = timestamp/1000;  //por causa do Panda Post que envia o timestamp 1000 vezes maior     
    var marker1;

    //Cria o marcador
    marker1 = new google.maps.Marker({
        position: new google.maps.LatLng(parseFloat(lat), parseFloat(lng)),
        map: map,
        title: String(tag_id)+" - ID da tag "+tag_name,
        icon:'/Home_files/warning_maker.png'
    });

    bounds.extend(marker1.position);
    if (cont==0) //nenhuma event_actions cadastrada previamente        
        cont=cont-1;

    cont=cont+1;
    markers_event_actions_array[cont]=marker1;
    
    lat_event_action = lat;
    lng_event_action = lng;
    timestamp_event_action = timestamp;
    tag_name_event_action = tag_name;
 
    //Evento de clicar no marcador (event_action)
    google.maps.event.addListener(marker1, 'click', (function(marker1) {
    return function() {
        var latlng = {lat: parseFloat(lat_event_action), lng: parseFloat(lng_event_action)};
        geocoder.geocode({'location': latlng}, function(results, status) {
            if (status === 'OK') {
                if (results[1]) {
                    map.setCenter(latlng);

                    var date = new Date(timestamp_event_action*1000); // converte para data
                    // Configura o conteúdo da janela do marcador
                    infowindow.setContent(
                        "<strong>Acionado o botão de ação da tag "+tag_name_event_action+"</strong>"+"<br><br>"+
                        results[0].formatted_address+"<br>"+
                        "("+lat_event_action+", "+lng_event_action+")"+"<br><br>"+
                        "Registrado em: " + date.toLocaleDateString("pt-BR") +" "+
                        + date.getHours() +":"+ date.getMinutes() +":"+ date.getSeconds()
                    );
                    // Abre a janela
                    infowindow.open(map, marker1);
                } else {
                    Materialize.toast('Nenhum resultado encontrado', 3500);
                }
            } else {
                Materialize.toast('Geocoder falhou devido a: ' + status + '. Local desconhecido.', 4000);
            }
        });
    }
    })(marker1));

    map.fitBounds(bounds);

    return;
}

/***** FIM - CREATE EVENT_ACTION APÓS RECEBER POST *****/


</script>