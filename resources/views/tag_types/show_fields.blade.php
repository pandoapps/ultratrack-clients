<!-- Picture Field -->
<div class="form-group">
    {!! Form::label('picture', ' ') !!}    
    <!-- Verifica se a foto está armazenada em um site, pois quando faz o sync as imagens vão estar armazenadas no site da UltraTrack -->
    <!-- h de http -->
    @if($tagType->picture[0] == 'h') 
        <img src="{{$tagType->picture}}" height="120px" width="100px"/>
    @else
        <img src="{{ '/storage/'.$tagType->picture }}" height="120px" width="100px"/>
    @endif
 </div>

<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $tagType->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $tagType->name !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $tagType->description !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $tagType->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $tagType->updated_at !!}</p>
</div>

