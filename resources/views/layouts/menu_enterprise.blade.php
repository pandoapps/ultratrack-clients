<br>

<li class="{{ Request::is('tags*') ? 'active' : '' }}">
    <a href="{!! url('/')  !!}"><i class="fa fa-map"></i><span>Página Inicial</span></a>
</li>
<ul class="sidebar-menu tree" data-widget="tree">
    <li class="treeview">
        <a href="#"><i class="glyphicon glyphicon-map-marker"></i> <span>Tags</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
            <li class="{{ Request::is('tags*') ? 'active' : '' }}">
                <a href="{!! route('tags.index') !!}"><i class="glyphicon glyphicon-pushpin"></i><span>Minhas Tags</span></a>
            </li>

            <li class="{{ Request::is('fields*') ? 'active' : '' }}">
                <a href="{!! route('fields.index') !!}"><i class="glyphicon glyphicon-edit"></i><span>Campos adicionais</span></a>
            </li>
        </ul>
    </li>

    <li class="treeview menu-open">
        <a href="#"><i class="fa  fa-users"></i> <span>Clientes</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
            <li class="{{ Request::is('clients*') ? 'active' : '' }}">
                <a href="{!! route('clients.index') !!}"><i class="fa fa-list"></i><span>Meus clientes</span></a>
            </li>

            <li class="{{ Request::is('statusClients*') ? 'active' : '' }}">
                <a href="{!! route('statusClients.index') !!}"><i class="glyphicon glyphicon-wrench"></i><span>Status dos Clientes</span></a>
            </li> 
        </ul>
    </li>
</ul>

<li class="{{ Request::is('enterprises*') ? 'active' : '' }}">
    <a href="{!! route('enterprises.index') !!}"><i class="glyphicon glyphicon-paperclip"></i><span>Meus dados cadastrais</span></a>
</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.create') !!}"><i class="glyphicon glyphicon-check"></i><span>Cadastrar outro usuário da<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; empresa</span></a>
</li>