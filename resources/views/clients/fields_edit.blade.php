<!-- Name Field -->

<div class="form-group col-sm-6">

    {!! Form::label('name', 'Nome:') !!}

    {!! Form::text('name', null, ['class' => 'form-control']) !!}

</div>


<!-- Email Field -->

<div class="form-group col-sm-6">

    {!! Form::label('email', 'Email:') !!}

    {!! Form::email('email', null, ['class' => 'form-control']) !!}

</div>


<!-- Phone Field -->

<div class="form-group col-sm-6">

    {!! Form::label('phone', 'Telefone:') !!}

    {!! Form::text('phone', null, ['class' => 'form-control']) !!}

</div>


<!-- Document Field -->

<div class="form-group col-sm-6">

    {!! Form::label('document', 'Documento:') !!}

    {!! Form::text('document', null, ['class' => 'form-control']) !!}

</div>


<!-- Picture Field -->

<div class="form-group col-sm-6">
    <br/>
    <input type="file" name="picture" id="picture" style="display:none;"/> 
    <label for="picture" class="btnfile btnfile-large">Trocar foto...</label> &nbsp; &nbsp; &nbsp;
       
    <!-- Verifica se a foto está armazenada em um site, pois quando faz o sync as imagens vão estar armazenadas no site da UltraTrack -->
    <!-- h de http -->
    @if($client->picture[0] == 'h')
            <img src="{{$client->picture}}" id="imagePreview" alt="" height="80px" width="80px"/>
    @else
            <img src="{{ '/storage/'.$client->picture }}" id="imagePreview" alt="" height="80px" width="80px"/>
    @endif
 
</div>

<!-- Status Client Id Field -->

<div class="form-group col-sm-6">

    {!! Form::label('status_client_id', 'Status:') !!}

    {!! Form::select('status_client_id', $status, null, ['class' => 'form-control']) !!}

    {!! Form::hidden('enterprise_id', $id_enterprise, null, ['class' => 'form-control']) !!}

</div>


<!-- Submit Field -->

<div class="form-group col-sm-12">
    <br/><br/>

    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}

    <a href="{!! route('clients.index') !!}" class="btn btn-default">Cancelar</a>

</div>
