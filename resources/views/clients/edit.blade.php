@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cliente
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($client, ['route' => ['clients.update', $client->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

                        @include('clients.fields_edit')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
   <script type="text/javascript" src="{!! asset('js/upload_file.js') !!}"></script>
@endsection