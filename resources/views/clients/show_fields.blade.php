<!-- Picture Field -->
<div class="form-group">
    {!! Form::label('picture', ' ') !!}    
    <!-- Verifica se a foto está armazenada em um site, pois quando faz o sync as imagens vão estar armazenadas no site da UltraTrack -->
    <!-- h de http -->
    @if($client->picture[0] == 'h') 
        <img src="{{$client->picture}}" height="120px" width="100px"/>
    @else
        <img src="{{ '/storage/'.$client->picture }}" height="120px" width="100px"/>
    @endif
 </div>

<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $client->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Nome:') !!}
    <p>{!! $client->name !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $client->email !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Telefone:') !!}
    <p>{!! $client->phone !!}</p>
</div>

<!-- Document Field -->
<div class="form-group">
    {!! Form::label('document', 'Documento:') !!}
    <p>{!! $client->document !!}</p>
</div>

<!-- Status Client Id Field -->
<div class="form-group">
    {!! Form::label('status_client_id', 'Status do Cliente:') !!}
    <p>{!! $name_status !!}</p>
</div>

<!-- Enterprise Id Field -->
<div class="form-group">
    {!! Form::label('enterprise_id', 'Empresa:') !!}
    <p>{!! $name_enterprise !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Criado em:') !!}
    <p>{!! $client->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Atualizado em:') !!}
    <p>{!! $client->updated_at !!}</p>
</div>

