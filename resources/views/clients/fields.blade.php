{!! csrf_field() !!}

<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Nome Completo:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-12">
    {!! Form::label('phone', 'Telefone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Document Field -->
<div class="form-group col-sm-12">
    {!! Form::label('document', 'Documento:') !!}
    {!! Form::text('document', null, ['class' => 'form-control']) !!}
</div>

<!-- Picture Field -->
<div class="form-group col-sm-12">
    <br/>
    <input type="file" name="picture" id="picture" style="display:none;"/> 
    <label for="picture" class="btnfile btnfile-large">Adicionar foto...</label> &nbsp; &nbsp; &nbsp;
    <img src="" id="imagePreview" alt="" height="80px" width="80px"/>
 
</div>

<!-- Status Client Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status_client_id', 'Status:') !!}
    {!! Form::select('status_client_id', $status, null, ['class' => 'form-control']) !!}
    {!! Form::hidden('enterprise_id', $enterprise_id, null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-12">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group has-feedback col-sm-12">
    {!! Form::label('password', 'Senha:') !!}
	<input type="password" class="form-control" name="password">
	<span class="glyphicon form-control-feedback"></span>
</div>

<div class="form-group has-feedback col-sm-12">
    {!! Form::label('password_confirmation', 'Insira a senha novamente:') !!}
	<input type="password" name="password_confirmation" class="form-control">
	<span class="glyphicon form-control-feedback"></span>
    </br>
</div>

<div class="form-group col-sm-12">
    <label>
        <input type="checkbox", required> Eu aceito os <a href="#">termos</a>
    </label>
   
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
  <center>  {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('home.index') !!}" class="btn btn-default">Cancelar</a></center>
</div>
<div class="form-group col-sm-12">
    </br><a href="{{ url('/login') }}" class="text-center">Eu já tenho cadastro</a>
</div>