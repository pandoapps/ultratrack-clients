<!DOCTYPE html>
<!--
  Name: Jednotka - Multipurpose Website HTML Template
  Author: Bublina Studio - www.bublinastudio.com, info@bublinastudio.com
  Version: 1.8.2
-->
<!--[if lt IE 9]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if !IE] -->
<html class=" js no-touch svg csstransforms csstransforms3d csstransitions" lang="pt-BR"><head>
      <title>Rastreamento de Idosos</title>

    <meta content="rastreamento, idosos, IOT, tags, localização, GPS" name="keywords">
    <meta content="Site da empresa de rastreamento de idosos" name="description">
    <meta content="http://www.pandoapps.com.br" name="author">
    <meta content="all" name="robots">
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <!--[if IE]> <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'> <![endif]-->
    <link href="/favicon" rel="shortcut icon" type="image/x-icon">
    <link href="/favicon" rel="apple-touch-icon-precomposed">
    <!-- / required stylesheets -->
    <link href="/Home_files/bootstrap.css" media="all" id="bootstrap" rel="stylesheet" type="text/css">
    <link href="/Home_files/jednotka_yellow.css" media="all" id="colors" rel="stylesheet" type="text/css">
    <!-- / not required stylesheets -->
    <link href="/Home_files/demo.css" media="all" rel="stylesheet" type="text/css">

    <style type="text/css">
      @import "compass/css3";
      @import url(https://fonts.googleapis.com/css?family=Raleway:100,400,700);
      body {
        background-repeat: no-repeat;
        background-size: cover;
        background-attachment: fixed;

        font-family: Raleway, Open Sans, Droid Sans, Roboto, arial, sans-serif;
      }
      .box {
        border-radius: 15px;
        background-color: rgba(23,23,23,0.7);
        border: 1px solid rgba(255, 255, 255, .3);
        padding: 20px;
        text-shadow: 0 1px 1px rgba(0, 0, 0, .4);
        transition: box-shadow .3s ease;
        .content {
          margin: auto;
        }
      }
      h1, h2, a, p {
        color: white;
        font-weight: 100;

        .tinted & {
          color: black;
          text-shadow: 0 1px 1px rgba(255, 255, 255, .2);
        }
      }
      h2 {
        font-size: 14px;
      }
    </style>

  </head>
  <body class="homepage">
      <header id="header">
        <div class="container">
          <div>
            <img class="pull-left" src="/Home_files/logo.jpg" alt="Logo" height="auto" width="80px" style="padding: 5px;"/>
          </div>
          <h1>              
            <a class="btn btn-success pull-right" style="margin-top: 0px; margin-bottom: 15px; margin-left:8px;" href="{!! route('clients.create')!!}">Seja nosso cliente</a>
            <a class="btn btn-warning pull-right" style="margin-top: 0px; margin-bottom: 15px; margin-left:8px;" href="{!! url('/login') !!}">Entrar no sistema</a>
          </h1>
        </div> 
      </header>
      <div id="main" role="main">       
        <!-- / carousel blur -->
        <div class="hero-carousel carousel-blur flexslider fade-loading" id="carousel" style="background-size: cover; background-color: #FFFFFF; background-image: url('/Home_files/carousel.jpg'); backdrop-filter: blur(5px); background-repeat: no-repeat; background-position: center; ">
          <ul class="slides">
            <li class="item" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
              <div class="container">
                <div class="row">
                  <div class="col-lg-12 animate box">
                      <h2 class="big fadeInRightBig animated">Rastreamento de Idosos</h2>
                      <p class="normal fadeInLeftBig animated">Garantindo a segurança de quem você gosta.</p>
                  </div>
                </div>
              </div> 
            </li>
          </ul>
        </div>
        <div id="main-content">
          <div class="container">
            <div class="panels-wrapper">
              <div class="row panels">
                  <div class="col-sm-4 panel-item">
                    <div class="panel panel-image">
                    <center><div class="panel-heading"><img class="img-responsive-sm" src="/Home_files/seguranca.png" alt="No picture found for this panel" width="100" height="100"></div></center>
                      <div class="panel-body">
                        <h3 class="panel-title">Segurança</h3>
                        <div class="content">Garantimos a segurança das informações de rastreio.</div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4 panel-item">
                    <div class="panel panel-image">
                      <center><div class="panel-heading"><img class="img-responsive-sm" src="/Home_files/praticidade.png" alt="No picture found for this panel" width="90" height="90"></div></center>
                      <div class="panel-body">
                        <h3 class="panel-title">Praticidade</h3>
                        <div class="content">O sistema de rastreio é prático e de fácil utilização.</div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4 panel-item">
                    <div class="panel panel-image">
                      <center><div class="panel-heading"><img class="img-responsive-sm" src="/Home_files/tempo_real.png" alt="No picture found for this panel" width="100" height="100"></div></center>
                      <div class="panel-body">
                        <h3 class="panel-title">Tempo Real</h3>
                        <div class="content">Em tempo real descubra a localização do idoso.</div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="page-header page-header-with-icon">
                  <i class="fa fa-suitcase"></i>
                  <h2>
                  <br/>
                      <small>Mais informações sobre nosso produto</small>
                  </h2>
                </div>
                <div class="row portfolio-boxes">
                    <div class="col-sm-3 col-xs-6 no-mb-t-xs portfolio-box">
                      <img class="img-responsive img-rounded center-block" src="/Home_files/produto.jpg" alt="No picture found for this panel'"  width="262" height="262"/>
                      <h3 class="title-responsive">Utilidade</h3>
                      <br/>
                      <div class="content">O Rastreamento de Idosos pode ajudar a previnir
                      que idosos com Alzheimer se percam de seus familiares.</div>
                    </div>
                    <div class="col-sm-3 col-xs-6 no-mb-t-xs portfolio-box">
                      <img class="img-responsive img-rounded center-block" src="/Home_files/map.png" alt="No picture found for this panel'"  width="262" height="262"/>
                      <h3 class="title-responsive">Rotas</h3>
                      <br/>
                      <div class="content">O Rastreamento de Idosos exibe as rotas percorridas pelo idoso
                      que está sendo rastreado em tempo real.</div>
                    </div>
                    <div class="col-sm-3 col-xs-6 no-mb-t-xs portfolio-box">
                      <img class="img-responsive img-rounded center-block" src="/Home_files/lugar_hora.jpg" alt="No picture found for this panel'"  width="262" height="262"/>
                      <h3 class="title-responsive">Em qualquer lugar e horário</h3>
                      <br/>
                      <div class="content">O Rastreamento de Idosos permite que, em qualquer lugar e horário quando conectados à Internet, familiares 
                       possam verificar onde o idoso está.</div>
                    </div>
                    <div class="col-sm-3 col-xs-6 no-mb-t-xs portfolio-box">
                      <img class="img-responsive img-rounded center-block" src="/Home_files/local.png" alt="No picture found for this panel'"  width="262" height="262"/>
                      <h3 class="title-responsive" style="margin-top: 23px">Tempo real e mobilidade</h3>
                      <br/>
                      <div class="content">O Rastreamento de Idosos exibe em tempo real a localização atual do idoso e permite que a pessoa se 
                      locomova enquanto faz uso do aplicativo.</div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="fade" id="scroll-to-top" style="bottom: 10px;">
          <i class="fa fa-chevron-up"></i>
        </div>
      </div>
      <!-- google maps starts here -->
      <div id="map" style="width: 100%; height: 400px; background-color: #CCC;"></div>
      <!-- google maps ends here -->
      <footer id="footer">
        <div id="footer-main">
          <div class="container">
            <div class="row">
              <div class="col-md-3 col-sm-6 info-box">
                <div class="logo-container">
                  <center><img src="/Home_files/logo.jpg" alt="Logo" height="auto" width="110px"></center>
                </div>
                <div class="content">
                <br/><br/><br/>
                <b>Rastreamento de Idosos:</b> Garantindo a segurança de quem você gosta.
                </div>
              </div>
              <div class="col-md-3 col-sm-6 info-box">
                <h2 class="title">Contato</h2>
                <div class="icon-boxes">
                  <div class="icon-box">
                    <div class="icon icon-wrap">
                      <i class="fa fa-map-marker"></i>
                    </div>
                    <div class="content">
                        Rua Cel. Rennó 07
                      <br>
                        37500-000,
                        Itajubá,
                        Minas Gerais
                    </div>
                  </div>
                  <div class="icon-box">
                    <div class="icon icon-wrap">
                      <i class="fa fa-phone"></i>
                    </div>
                    <div class="content">
                      3600-0000
                    </div>
                  </div>
                    <div class="icon-box">
                      <div class="icon icon-wrap">
                        <i class="fa fa-envelope"></i>
                      </div>
                      <div class="content"><a href="mailto:site@gmail.com">empresa@gmail.com</a></div>
                    </div>
                    <div class="icon-box">
                      <div class="icon icon-wrap">
                        <i class="fa fa-globe"></i>
                      </div>
                      <div class="content"><a href="meusite.com.br">site-empresa.com.br</a></div>
                    </div>
                  <div class="links">
                      <a class="btn btn-circle btn-medium-light btn-sm" href="https://pt-br.facebook.com/">
                        <i class="fa fa-facebook text-dark"></i>
                      </a>
                      <a class="btn btn-circle btn-medium-light btn-sm" href="https://twitter.com/?lang=pt">
                        <i class="fa fa-twitter text-dark"></i>
                      </a>
                      <a class="btn btn-circle btn-medium-light btn-sm" href="https://br.linkedin.com/">
                        <i class="fa fa-linkedin-square text-dark"></i>
                      </a>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 info-box social-box">
                <h2 class="title">Sobre Nós</h2>
                <div class="content">
                    Empresa de rastreamento de idosos.
                </div>
              </div>
              <div class="col-md-3 col-sm-6 info-box">
                <h2 class="title">Desenvolvido por</h2>
                <img src="/Home_files/pando_white.png" alt="Pandô APPs" height="auto" width="160">
              </div>
            </div>
          </div>
        </div>
      </footer>

    <!-- / required javascripts -->
    <script async="" src="/Home_files/analytics.js"></script><script src="/Home_files/jquery_002.js" type="text/javascript"></script>
    <script src="/Home_files/jquery_005.js" type="text/javascript"></script>
    <script src="/Home_files/bootstrap.js" type="text/javascript"></script>
    <script src="/Home_files/modernizr.js" type="text/javascript"></script>
    <script src="/Home_files/twitter-bootstrap-hover-dropdown.js" type="text/javascript"></script>
    <script src="/Home_files/retina.js" type="text/javascript"></script>
    <script src="/Home_files/jquery_007.js" type="text/javascript"></script>
    <script src="/Home_files/jquery_004.js" type="text/javascript"></script>
    <script src="/Home_files/jquery_008.js" type="text/javascript"></script>
    <script src="/Home_files/jquery_006.js" type="text/javascript"></script>
    <script src="/Home_files/jquery_003.js" type="text/javascript"></script>
    <script src="/Home_files/countdown.js" type="text/javascript"></script>
    <script src="/Home_files/nivo-lightbox.js" type="text/javascript"></script>
    <script src="/Home_files/jquery.js" type="text/javascript"></script>
    <script src="/Home_files/jednotka.js" type="text/javascript"></script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8huuwkA2UKKZ7yFb4ygql_4BFiaGyHtI&callback=initMap"></script>
    <script>
      function initMap() {
          var mapCanvas = document.getElementById('map');
          var latLng = new google.maps.LatLng('-22.4253360', '-45.4528560');
          var mapOptions = {
              center: latLng,
              zoom: 17,
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              zoomControl: true,
              scrollwheel: false,
              draggable: true,
              streetViewControl: true,
          }
          var map = new google.maps.Map(mapCanvas, mapOptions);

          var marker = new google.maps.Marker({
              position: latLng,
              animation: google.maps.Animation.BOUNCE,
          });
          var marker_text = new google.maps.InfoWindow({
            content: "Empresa Rastreamento de Idosos" 
          });

          google.maps.event.addListener(marker, "click", function(e) {
            marker_text.open(map, this);
          });

          // To add the marker to the map, call setMap();
          marker.setMap(map);
      }
    </script>

    <!-- / not required javascripts -->
    <!-- <script src="/Home_files/demo.js" type="text/javascript"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-44249116-1', 'bublinastudio.com');
      ga('send', 'pageview');
    </script> -->

    </body>
</html>
