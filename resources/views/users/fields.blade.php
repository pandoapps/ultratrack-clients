<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nome:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Telefone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Picture Field -->
<div class="form-group col-sm-6">
    <br/>
    <input type="file" name="picture" id="picture" style="display:none;"/> 
    <label for="picture" class="btnfile btnfile-large">Trocar foto...</label> &nbsp; &nbsp; &nbsp;
    <img src="{{ '/storage/'.$user->picture }}" id="imagePreview" alt="" height="80px" width="80px"/>
    <div id="image-holder"></div>
</div>

<div class="col-sm-12">
    </br>
    <u><h3>Alteração da senha</h3></u>
    </br>
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Senha:') !!}
    {!! Form::password('password', ['class' => 'form-control','placeholder'=>'Não alterar esse campo se não desejar alterar a senha']) !!}
</div>

<!-- PasswordConfirmation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password_confirmation', 'Insira a senha novamente:') !!}
    {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder'=>'Não alterar esse campo se não desejar alterar a senha']) !!}
</div>

<!-- Submit Field -->
<center>
    <div class="form-group col-sm-12">
        </br></br>
        {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('tags.index') !!}" class="btn btn-default">Cancelar</a>
    </div>
</center>
