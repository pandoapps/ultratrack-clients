{!! csrf_field() !!}

<div class="col-md-3"></div>
<div class="col-md-6">
	<div class="form-group has-feedback">
		<input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Nome Completo">
		<span class="glyphicon glyphicon-user form-control-feedback"></span>

	</div>

	<div class="form-group has-feedback">
		<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
		<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
	</div>

	<div class="form-group has-feedback">
		<input type="password" class="form-control" name="password" placeholder="Senha">
		<span class="glyphicon glyphicon-lock form-control-feedback"></span>
	</div>

	<div class="form-group has-feedback">
		<input type="password" name="password_confirmation" class="form-control" placeholder="Insira a senha novamente">
		<span class="glyphicon glyphicon-lock form-control-feedback"></span>
	</div>

	<!-- Picture Field -->
	<div class="form-group has-feedback">
		<br/>
		<input type="file" name="picture" id="picture" style="display:none;"/> 
		<label for="picture" class="btnfile btnfile-large">Adicionar foto...</label> &nbsp; &nbsp; &nbsp;
		<img src="" id="imagePreview" alt="" height="80px" width="80px"/>
		<br/><br/>
	</div>	
	</br>
	<center>
		<!-- Submit Field -->
		<div class="form-group">
			{!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
			<a href="{!! route('tags.index') !!}" class="btn btn-default">Cancelar</a>
		</div>
	</center>	
</div>


