@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Cadastro de usuário da empresa
        </h1>
    </section>

    <div class="content">
	@include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'users.store','enctype' => 'multipart/form-data']) !!}

                        @include('users.fields_create')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div> 
    <script type="text/javascript" src="{!! asset('js/upload_file.js') !!}"></script>
@endsection
