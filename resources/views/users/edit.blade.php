@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Meus dados cadastrais
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       @include('flash::message')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

                        @include('users.fields')

                   {!! Form::close() !!}
               </div>
               </br> </br>
                <div class="form-group col-sm-12">
                    <h1 class="pull-right">                
                    {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i><p>Desejo excluir minha conta</p>', [
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-xs',
                                'onclick' => "return confirm('Você realmente deseja fazer essa exclusão?')"
                            ]) !!}
                    {!! Form::close() !!}
                    </h1> 
                </div> 
                </br>
           </div>
       </div>
   </div>
   <script type="text/javascript" src="{!! asset('js/upload_file.js') !!}"></script>
@endsection