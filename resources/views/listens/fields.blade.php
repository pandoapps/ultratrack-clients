<!-- Speed Field -->
<div class="form-group col-sm-6">
    {!! Form::label('speed', 'Speed:') !!}
    {!! Form::text('speed', null, ['class' => 'form-control']) !!}
</div>

<!-- Temperature Field -->
<div class="form-group col-sm-6">
    {!! Form::label('temperature', 'Temperature:') !!}
    {!! Form::text('temperature', null, ['class' => 'form-control']) !!}
</div>

<!-- Batery Field -->
<div class="form-group col-sm-6">
    {!! Form::label('batery', 'Batery:') !!}
    {!! Form::text('batery', null, ['class' => 'form-control']) !!}
</div>

<!-- Last Point Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_point', 'Last Point:') !!}
    {!! Form::text('last_point', null, ['class' => 'form-control']) !!}
</div>

<!-- Tag Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tag_id', 'Tag Id:') !!}
    {!! Form::select('tag_id', ['id' => ' Tag'], null, ['class' => 'form-control']) !!}
</div>

<!-- Listened At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('listened_at', 'Listened At:') !!}
    {!! Form::text('listened_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('listens.index') !!}" class="btn btn-default">Cancel</a>
</div>
