<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $listen->id !!}</p>
</div>

<!-- Speed Field -->
<div class="form-group">
    {!! Form::label('speed', 'Speed:') !!}
    <p>{!! $listen->speed !!}</p>
</div>

<!-- Temperature Field -->
<div class="form-group">
    {!! Form::label('temperature', 'Temperature:') !!}
    <p>{!! $listen->temperature !!}</p>
</div>

<!-- Batery Field -->
<div class="form-group">
    {!! Form::label('batery', 'Batery:') !!}
    <p>{!! $listen->batery !!}</p>
</div>

<!-- Last Point Field -->
<div class="form-group">
    {!! Form::label('last_point', 'Last Point:') !!}
    <p>{!! $listen->last_point !!}</p>
</div>

<!-- Tag Id Field -->
<div class="form-group">
    {!! Form::label('tag_id', 'Tag Id:') !!}
    <p>{!! $listen->tag_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $listen->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $listen->updated_at !!}</p>
</div>

<!-- Listened At Field -->
<div class="form-group">
    {!! Form::label('listened_at', 'Listened At:') !!}
    <p>{!! $listen->listened_at !!}</p>
</div>

