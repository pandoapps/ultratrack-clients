<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nome:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Descrição:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- possui campos adicionais cadastrados -->
@if ($fields) 
        @foreach ($fields as $field)
        <div class="form-group col-sm-6">
             {!! Form::label('field_name', $field->name) !!}
             {!! Form::text('array[]', $field->pivot->value, ['class' => 'form-control']) !!}
        </div>
        @endforeach
    @endif

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('tags.index') !!}" class="btn btn-default">Cancelar</a>
</div>
