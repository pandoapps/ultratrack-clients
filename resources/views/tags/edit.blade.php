@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Tag
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($tag, ['route' => ['tags.update', $tag->id], 'method' => 'patch']) !!}

                   @role('client')

                         @include('tags.client_fields')
                
                   @endrole

                   @role('enterprise')

                         @include('tags.fields')
                
                   @endrole

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection