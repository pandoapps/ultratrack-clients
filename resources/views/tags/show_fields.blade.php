<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'ID da tag:') !!}
    <p>{!! $tag->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Nome:') !!}
    <p>{!! $tag->name !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Descrição:') !!}
    <p>{!! $tag->description !!}</p>
</div>

<!-- Last Point Field -->
<div class="form-group">
    {!! Form::label('last_point', 'Último ponto:') !!}
    <p>{!! $tag->last_point !!}</p>
</div>

<!-- Degrees Field -->
<div class="form-group">
    {!! Form::label('degrees', 'Graus:') !!}
    <p>{!! $tag->degrees !!}</p>
</div>

<!-- Enterprise Id Field -->
<div class="form-group">
    {!! Form::label('enterprise_id', 'Empresa:') !!}
    <p>{!! $name_enterprise !!}</p>
</div>

<!-- Client Id Field -->
<div class="form-group">
    {!! Form::label('client_id', 'Cliente:') !!}
    <p>{!! $name_client !!}</p>
</div>

<!-- Status Tag Id Field -->
<div class="form-group">
    {!! Form::label('status_tag_id', 'Status da tag:') !!}
    <p>{!! $status_tags !!}</p>
</div>

<!-- Tag Type Id Field -->
<div class="form-group">
    {!! Form::label('tag_type_id', 'Tipo da tag:') !!}
    <p>{!! $tag_type !!}</p>
</div>

<!-- possui campos extras cadastrados -->
@if ($fields) 
        @foreach ($fields as $field)
        <div class="form-group">
             {!! Form::label('field_name', $field->name.':') !!}
             <p>{!! $field->pivot->value !!}</p>
        </div>
        @endforeach
    @endif

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Criada em:') !!}
    <p>{!! $tag->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Atualizada em:') !!}
    <p>{!! $tag->updated_at !!}</p>
</div>

