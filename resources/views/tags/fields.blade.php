<!-- Enterprise Id Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('enterprise_id', 'Empresa:') !!}
    {!! Form::text('enterprise_id',$name_enterprise, ['class' => 'form-control', 'disabled'  => '']) !!}
    {!! Form::hidden('enterprise_id', $id_enterprise, null, ['class' => 'form-control']) !!}
    <br>
</div>

<!-- Last Point Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_point', 'Último ponto:') !!}
    {!! Form::text('last_point', null, ['class' => 'form-control', 'disabled'  => '']) !!}
</div>

<!-- Degrees Field -->
<div class="form-group col-sm-6">
    {!! Form::label('degrees', 'Graus:') !!}
    {!! Form::text('degrees', null, ['class' => 'form-control', 'disabled'  => '']) !!}
    <br><br>
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nome:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Descrição:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Client Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('client_id', 'Cliente:') !!}
    {!! Form::select('client_id', $clients, null, ['class' => 'form-control']) !!}
</div>

<!-- Status Tag Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status_tag_id', 'Status da tag:') !!}
    {!! Form::select('status_tag_id', $status_tags, null, ['class' => 'form-control']) !!}
</div>

<!-- possui campos adicionais cadastrados -->
@if ($fields) 
    @foreach ($fields as $field)
    <div class="form-group col-sm-6">
        {!! Form::label('field_name', $field->name) !!}
        {!! Form::text('array[]', $field->pivot->value, ['class' => 'form-control']) !!}
    </div>
    @endforeach
@endif

<!-- Tag Type Id Field -->
<div class="form-group col-sm-6">
    {!! Form::hidden('tag_type_id', $tag->tag_type_id, null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <br><br>
    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('tags.index') !!}" class="btn btn-default">Cancelar</a>
</div>