@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Campos adicionais para especificação das tags</h1>
        <h1 class="pull-right">
           <a class="btn btn-success pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('fields.create') !!}">Adicionar novo campo</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('fields.table')
            </div>
        </div>
        <center><a href="{!! route('tags.index') !!}" class="btn btn-primary">Voltar</a></center>
    </div>
@endsection

