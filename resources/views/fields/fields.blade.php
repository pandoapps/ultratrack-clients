<!-- Enterprise Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('enterprise_id', 'Empresa:') !!}
    {!! Form::text('enterprise_id', $name_enterprise, ['class' => 'form-control', 'disabled'  => '']) !!}
    {!! Form::hidden('enterprise_id', $id_enterprise, null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-8">
    <br>
    {!! Form::label('name', 'Nome:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <br><br>
    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('fields.index') !!}" class="btn btn-default">Cancelar</a>
</div>
