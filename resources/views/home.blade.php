@extends('layouts.app')

<!-- HomeController que filtra as tags que o usuário poderá visualizar -->
@section('content')
    <section class="content-header">
        <h1>
            {{$enterprise_name}} - Tags
        </h1>
    </section>
    <style>
    #map-canvas{
        width: 100%;
        height: 280px;
    }
    </style>

    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&libraries=places&language=pt-br" type="text/javascript"></script>

     <!-- Include Date Range Picker -->
     <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

     <!-- Compiled and minified CSS -->
    <link rel="stylesheet" type="text/css" href="/css/materialize.css"/>     
     
    <div class="content">
     <div class="box box-primary">
         <div class="box-body">
             <div class="row" style="padding-left: 20px">
                 
                 <div class="col-sm-12">
                     <div class="col-sm-9">
                         <div id="map-canvas"></div>
                     </div><!-- /Map div -->
                     <div class="col-sm-3 well well-sm pre-scrollable" style="height:280px;">
                         @foreach($tags as $key=>$tag)
                             <div class="input-group marker-div" style="padding:3px 0">
                                 <span class="input-group-addon">
                                     <input id="{{$key}}" type="checkbox" onclick="toggleGroup({{$key}}, {{$tag->id}})" checked aria-label="...">
                                 </span>
                                 <span type="text" class="form-control" aria-label="..." style="float:none">
                                     {{$tag->name}}
                                 </span>
                             </div><!-- /input-group -->
                         @endforeach
                     </div><!-- /Tags list -->
                 </div><!-- /col-sm-12 -->
                 
                 <div class="col-sm-12">
                     <hr>
                      <!-- DateRange Picker -->
                     <div class="form-group has-feedback" style="width: 45%">
                         {!! Form::label('periodo', 'Período:') !!}
                         {!! Form::text('daterange', null, ['class' => 'form-control', 'id'=> 'daterange']) !!}
                         <i class="glyphicon glyphicon-calendar fa fa-calendar form-control-feedback"></i>
                     </div>
                      <!-- /DateRange Picker -->
                     <div class="input-group">
                        {!! Form::label('rotas', 'Tipos de rotas:') !!}<br/>
                        <div class="radioBtn btn-group">
                            <label class="radio-inline">
                                <input class="" type="radio" value="drive" name="radio-route" checked>Dirigindo
                            </label>
                            <label class="radio-inline">
                                <input class="" type="radio" value="walk" name="radio-route">Caminhando
                            </label>
                            <label class="radio-inline">
                                <input class="" type="radio" value="line" name="radio-route">Linha reta
                            </label>
                        </div>
                    </div>
                    <div class="input-group">
                        <br/>
                        {!! Form::label('rotas_config', 'Ao traçar rotas do tipo Dirigindo ou Caminhando:') !!}<br/>
                        <div class="radioBtn btn-group">
                            <label class="radio-inline">
                                <input class="" type="radio" value="show" name="radio-route-config" checked>Exibir marcadores
                            </label>
                            <label class="radio-inline">
                                <input class="" type="radio" value="dontshow" name="radio-route-config">Não exibir marcadores
                            </label>
                        </div>
                    </div>
                    <br/> 
                     <div class="form-group col-sm-12" style="text-align:center;">
                         <input class="btn btn-primary" type="button" id="routebtn" value="Traçar Rota" />
                     </div>
                     <br/>
                     <div class="form-group col-sm-12">
                         <input class="btn btn-default" type="button" id="relatoriobtn" value="Exibir Relatório" />
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>
@endsection

@section('scripts')

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script type="text/javascript" src="{!! asset('js/map_style.js') !!}"></script>
<script type="text/javascript" src="/js/AutobahnJS.js"></script>

@include('map.relatorio')
@include('map.home_map')
@include('map.map_real_time')


@endsection