@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Status Tag
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($statusTag, ['route' => ['statusTags.update', $statusTag->id], 'method' => 'patch']) !!}

                        @include('status_tags.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection