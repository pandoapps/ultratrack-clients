@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Status do Cliente
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($statusClient, ['route' => ['statusClients.update', $statusClient->id], 'method' => 'patch']) !!}

                        @include('status_clients.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection