<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $eventAction->id !!}</p>
</div>

<!-- Last Point Field -->
<div class="form-group">
    {!! Form::label('last_point', 'Last Point:') !!}
    <p>{!! $eventAction->last_point !!}</p>
</div>

<!-- Tag Id Field -->
<div class="form-group">
    {!! Form::label('tag_id', 'Tag Id:') !!}
    <p>{!! $eventAction->tag_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $eventAction->created_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Listened At:') !!}
    <p>{!! $eventAction->listened_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $eventAction->updated_at !!}</p>
</div>

