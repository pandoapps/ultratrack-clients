<?php

return [

    'type' => '[0,1]Tipo de caixa|[2,Inf]Tipo de caixas',
    'id' => 'Id',
    'name' => 'Nome do tipo',
    'description' => 'Descrição',
    'created_at' => 'Criado em',
    'updated_at' => 'Atualizado em',

];