<?php

return [

    'box' => '[0,1] Caixa | [2,Inf] Caixas',
    'id' => 'Id',
    'name' => 'Nome',
    'description' => 'Descrição',
    'site_id' => 'Site',
    'type_id' => 'Tipo da caixa',
    'picture_url' => 'Imagem',
    'picture_url_name' => 'Nome da imagem',
    'created_at' => 'Criado em',
    'updated_at' => 'Atualizado em',

];
