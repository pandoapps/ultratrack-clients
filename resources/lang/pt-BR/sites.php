<?php

return [

    'site' => '[0,1]Site|[2,Inf]Sites',
    'id' => 'Id',
    'site_name' => 'Nome do site',
    'company_name' => 'Nome da empresa',
    'slogan' => 'Slogan',
    'title' => 'Título do site',
    'logo' => 'Logo da empresa',
    'keywords' => 'Palavras-chave',
    'description' => 'Descrição da empresa',
    'products_description' => 'Descrição dos produtos',
    'fav_icon' => 'Favicon',
    'main_image' => 'Imagem principal',
    'latitude' => 'Latitude',
    'longitude' => 'Longitude',
    'address' => 'Endereço',
    'zipcode' => 'CEP',
    'city' => 'Cidade',
    'state' => 'Estado',
    'phone_number_1' => 'Telefone',
    'phone_number_2' => 'Telefone alternativo',
    'email' => 'E-mail',
    'facebook' => 'Facebook',
    'twitter' => 'Twitter',
    'other_social_network' => 'Outra rede social',
    'free_text' => 'Texto livre',
    'created_at' => 'Criado em',
    'updated_at' => 'Atualizado em',
    
    
];