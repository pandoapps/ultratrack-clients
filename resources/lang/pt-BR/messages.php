<?php

return [

    'saved' => '{0} salvo com sucesso! | {1} salva com sucesso!',
    'updated' => '{0} atualizado com sucesso! | {1} atualizada com sucesso!',
    'deleted' => '{0} deletado com sucesso! | {1} deletada com sucesso!',
    'not_found' => '{0} não encontrado. | {1} não encontrada.',

];