document.getElementById("picture").onchange = function () {
    
    var imgPath = $(this)[0].value;
    var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();

    if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
        if (typeof (FileReader) != "undefined") {

            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imagePreview').attr('src', e.target.result);
            }

            reader.readAsDataURL($(this)[0].files[0]);

        } else {
            alert("Este navegador não suporta FileReader.");
        }
    } else {
        alert("Por favor, selecione apenas imagens e que possuam um dos seguintes formatos: gif, png, jpg ou jpeg.");
    }
};