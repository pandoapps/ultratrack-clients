<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
  return redirect('/home');
}); 

Route::get('/home', 'HomeController@index')->name('home.index');  ;

Route::get('/clients/create', 'ClientController@create')->name('clients.create');;
Route::post('/clients', 'ClientController@store')->name('clients.store');

//Route::group(['prefix' => 'admin', 'middleware' => ['role:admin']], function() {

Route::group(['middleware' => ['role:enterprise|client']], function() {

    Route::resource('tags', 'TagController');

    Route::resource('users', 'UserController'); 
     
    Route::resource('eventActions', 'EventActionController');

    //Ajax - Tag Tracker
    Route::get('/track_tag', 'TagController@tag_tracker')->name('tag_tracker'); 
    
    Route::group(['middleware' => ['role:enterprise']], function() {

        Route::resource('statusTags', 'StatusTagController');
      
        Route::resource('statusClients', 'StatusClientController');

        Route::resource('fields', 'FieldController');

        Route::get('/clients', 'ClientController@index')->name('clients.index');
        Route::get('/clients/{client}', 'ClientController@show')->name('clients.show');
        Route::get('/clients/{client}/edit', 'ClientController@edit')->name('clients.edit');        
        Route::delete('/clients/{client}', 'ClientController@destroy')->name('clients.destroy');
        Route::patch('clients/{client}', 'ClientController@update')->name('clients.update');  

        Route::resource('enterprises', 'EnterpriseController');
   
    });   
});

Auth::routes(); // login, logout

/*    
Route::resource('statusEnterprises', 'StatusEnterpriseController');

Route::resource('messages', 'MessageController');

Route::resource('tagTypes', 'TagTypeController');

Route::resource('configs', 'ConfigController');
*/