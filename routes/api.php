<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('listens/store', '\App\Http\Controllers\ListenController@store')->name('api.listens.store');

Route::post('event_actions/store', '\App\Http\Controllers\EventActionController@store')->name('api.event_actions.store');

Route::post('sync/sync_db', '\App\Http\Controllers\SyncController@sync_db')->name('api.sync.sync_db');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
