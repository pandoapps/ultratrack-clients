<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Config
 * @package App\Models
 * @version August 30, 2017, 5:29 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection ces
 * @property \Illuminate\Database\Eloquent\Collection cts
 * @property string name
 * @property string description
 * @property boolean is_active
 * @property string value
 */
class Config extends Model
{
    use SoftDeletes;

    public $table = 'configs';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'id',
        'name',
        'description',
        'is_active',
        'value',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'description' => 'string',
        'is_active' => 'boolean',
        'value' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'description' => 'required',
        'is_active' => 'required',
        'value' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function enterprises()
    {
        return $this->belongsToMany(\App\Models\Enterprise::class, 'ces', 'enterprise_id', 'config_id', 'value');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function tags()
    {
        return $this->belongsToMany(\App\Models\Tag::class, 'cts', 'tag_id', 'config_id', 'value');
    }
}
