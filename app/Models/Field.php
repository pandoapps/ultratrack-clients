<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Field
 * @package App\Models
 * @version August 30, 2017, 5:47 pm UTC
 *
 * @property \App\Models\Enterprise enterprise
 * @property \Illuminate\Database\Eloquent\Collection tagFields
 * @property string name
 * @property integer enterprise_id
 */
class Field extends Model
{
    use SoftDeletes;

    public $table = 'fields';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'enterprise_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'enterprise_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function enterprise()
    {
        return $this->belongsTo(\App\Models\Enterprise::class, 'enterprise_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function tags()
    {
        return $this->belongsToMany(\App\Models\Tag::class, 'tag_fields', 'tag_id', 'field_id')->withPivot('value');
    }
}
