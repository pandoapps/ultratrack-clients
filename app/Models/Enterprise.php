<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Enterprise
 * @package App\Models
 * @version September 12, 2017, 12:08 pm UTC
 *
 * @property \App\Models\StatusEnterprise statusEnterprise
 * @property \Illuminate\Database\Eloquent\Collection User
 * @property \Illuminate\Database\Eloquent\Collection ces
 * @property \Illuminate\Database\Eloquent\Collection Client
 * @property \Illuminate\Database\Eloquent\Collection Field
 * @property string name
 * @property string email
 * @property string phone
 * @property string document
 * @property string logo
 * @property string field_key
 * @property string endpoint
 * @property integer status_enterprise_id
 */
class Enterprise extends Model
{
    use SoftDeletes;

    public $table = 'enterprises';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'id',
        'name',
        'email',
        'phone',
        'document',
        'logo',
        'field_key',
        'endpoint',
        'status_enterprise_id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'document' => 'string',
        'logo' => 'string',
        'field_key' => 'string',
        'endpoint' => 'string',
        'status_enterprise_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required',
        'phone' => 'required',
        'document' => 'required',
        'logo' => 'required',
        'field_key' => 'required',
        'endpoint' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function statusEnterprise()
    {
        return $this->belongsTo(\App\Models\StatusEnterprise::class, 'status_enterprise_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function users()
    {
        return $this->hasMany(\App\Models\User::class, 'enterprise_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function configs()
    {
        return $this->belongsToMany(\App\Models\Config::class, 'ces', 'enterprise_id', 'config_id', 'value');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function clients()
    {
        return $this->hasMany(\App\Models\Client::class, 'enterprise_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function fields()
    {
        return $this->hasMany(\App\Models\Field::class, 'enterprise_id');
    }
}
