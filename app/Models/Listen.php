<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Listen
 * @package App\Models
 * @version September 20, 2017, 3:43 pm BRT
 *
 * @property \App\Models\Tag tag
 * @property string speed
 * @property string temperature
 * @property string batery
 * @property point last_point
 * @property integer tag_id
 * @property timestamp listened_at
 */
class Listen extends Model
{

    public $table = 'listens';

    public $fillable = [
        'id',
        'speed',
        'temperature',
        'batery',
        'last_point',
        'tag_id',
        'listened_at',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'speed' => 'string',
        'temperature' => 'string',
        'batery' => 'string',
        'tag_id' => 'integer',
        'listened_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'speed' => 'required',
        'temperature' => 'required',
        'batery' => 'required',
        'last_point' => 'required',
        'tag_id' => 'required',
        'listened_at' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function tag()
    {
        return $this->belongsTo(\App\Models\Tag::class, 'tag_id', 'id');
    }
}
