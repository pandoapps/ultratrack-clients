<?php namespace App\Models;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{

    public $fillable = [
        'id',
        'display_name',
        'name',
        'description',
    ];
    
}