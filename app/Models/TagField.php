<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TagField
 * @package App\Models
 * @version August 30, 2017, 5:47 pm UTC
 *
 * @property integer tag_id
 * @property integer field_id
 * @property string value
 */
class TagField extends Model
{
    use SoftDeletes;

    public $table = 'tag_fields';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'tag_id',
        'field_id',
        'value'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'tag_id' => 'integer',
        'field_id' => 'integer',
        'value' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'value' => 'required'
    ];

    
}
