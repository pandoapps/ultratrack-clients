<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class StatusEnterprise
 * @package App\Models
 * @version August 30, 2017, 5:06 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection Enterprise
 * @property string name
 * @property string description
 */
class StatusEnterprise extends Model
{
    use SoftDeletes;

    public $table = 'status_enterprises';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'id',
        'name',
        'description',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id',
        'name' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'description' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function enterprises()
    {
        return $this->hasMany(\App\Models\Enterprise::class, 'status_enterprise_id');
    }
}
