<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

/**
 * Class EventAction
 * @package App\Models
 * @version August 30, 2017, 5:28 pm UTC
 *
 * @property \App\Models\Tag tag
 * @property point last_point
 * @property integer tag_id
 */
class EventAction extends Model
{
    use SoftDeletes;

    public $table = 'event_actions';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'id',
        'last_point',
        'tag_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'listened_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'tag_id' => 'integer',
        'listened_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
     public static $rules = [
        'lat' => 'required',
        'lng' => 'required',
        'tag_id' => 'required',
        'listened_at' => 'required'
    ];


    public function getCoord(){
        $query = DB::select(
            'SELECT ST_AsGeoJSON(last_point) 
            from event_actions 
            where id = ' . $this->id . 'limit 1'
        );    
        $array = json_decode($query[0]->st_asgeojson)->coordinates;
        return $array;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function tag()
    {
        return $this->belongsTo(\App\Models\Tag::class, 'tag_id', 'id');
    }
}
