<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

/**
 * Class Tag
 * @package App\Models
 * @version August 30, 2017, 5:27 pm UTC
 *
 * @property \App\Models\Enterprise enterprise
 * @property \App\Models\Client client
 * @property \App\Models\StatusTag statusTag
 * @property \App\Models\TagType tagType
 * @property \Illuminate\Database\Eloquent\Collection Listen
 * @property \Illuminate\Database\Eloquent\Collection EventAction
 * @property \Illuminate\Database\Eloquent\Collection cts
 * @property \Illuminate\Database\Eloquent\Collection tagFields
 * @property string name
 * @property string description
 * @property point last_point
 * @property string degrees
 * @property integer enterprise_id
 * @property integer client_id
 * @property integer status_tag_id
 * @property integer tag_type_id
 */
class Tag extends Model
{
    use SoftDeletes;

    public $table = 'tags';    

    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'name' => 10,
        ]
    ];

    protected $dates = ['deleted_at'];

    public $fillable = [
        'id',
        'name',
        'description',
        'last_point',
        'degrees',
        'enterprise_id',
        'client_id',
        'status_tag_id',
        'tag_type_id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'description' => 'string',
        'degrees' => 'string',
        'enterprise_id' => 'integer',
        'client_id' => 'integer',
        'status_tag_id' => 'integer',
        'tag_type_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'description' => 'required'
    ];

    public function getCoord(){
        $query = DB::select(
            'SELECT ST_AsGeoJSON(last_point) 
            from tags 
            where id = ' . $this->id . 'limit 1'
        );    
        $array = json_decode($query[0]->st_asgeojson)->coordinates;
        return $array;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function enterprise()
    {
        return $this->belongsTo(\App\Models\Enterprise::class, 'enterprise_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function client()
    {
        return $this->belongsTo(\App\Models\Client::class, 'client_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function statusTag()
    {
        return $this->belongsTo(\App\Models\StatusTag::class, 'status_tag_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function tagType()
    {
        return $this->belongsTo(\App\Models\TagType::class, 'tag_type_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function listens()
    {
        return $this->hasMany(\App\Models\Listen::class, 'tag_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function eventActions()
    {
        return $this->hasMany(\App\Models\EventAction::class, 'tag_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function configs()
    {
        return $this->belongsToMany(\App\Models\Config::class, 'cts', 'tag_id', 'config_id', 'value');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function fields()
    {
        return $this->belongsToMany(\App\Models\Field::class, 'tag_fields', 'tag_id', 'field_id')->withPivot('value');
    }

}
