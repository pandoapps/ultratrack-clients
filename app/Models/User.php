<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Zizaco\Entrust\Traits\EntrustUserTrait;

/**
 * Class User
 * @package App\Models
 * @version August 31, 2017, 6:07 pm UTC
 *
 * @property \App\Models\Enterprise enterprise
 * @property \App\Models\Client client
 * @property \App\Models\UserType userType
 * @property string name
 * @property string email
 * @property string password
 * @property string phone
 * @property string picture
 * @property integer client_id
 */
class User extends Authenticatable
{
    use SoftDeletes, EntrustUserTrait {
        SoftDeletes::restore insteadof EntrustUserTrait;
        EntrustUserTrait::restore insteadof SoftDeletes;
    }

    public $table = 'users';
    
    protected $dates = ['deleted_at'];


    public $fillable = [
        'id',
        'name',
        'email',
        'password',
        'phone',
        'picture',
        'enterprise_id',
        'client_id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'phone' => 'string',
        'picture' => 'string',
        'enterprise_id' => 'integer',
        'client_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:255',
        'email' => 'required|email|max:255|unique:users',
        'password' => 'required|string|min:6|confirmed',
        'phone' => '',
        'picture' => 'required'
    ];

    public static $rules_update = [
        'name' => 'required|string|max:255',
        'email' => 'required|email|max:255',
        'password' => 'confirmed',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function enterprise()
    {
        return $this->belongsTo(\App\Models\Enterprise::class, 'enterprise_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function client()
    {
        return $this->belongsTo(\App\Models\Client::class, 'client_id', 'id');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
     protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
        return (isset($this->userType) && $this->userType->name == 'Admin') ? true : false; 
    }
}
