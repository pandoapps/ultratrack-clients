<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Client
 * @package App\Models
 * @version August 31, 2017, 5:58 pm UTC
 *
 * @property \App\Models\StatusClient statusClient
 * @property \App\Models\Enterprise enterprise
 * @property \Illuminate\Database\Eloquent\Collection Tag
 * @property \Illuminate\Database\Eloquent\Collection User
 * @property string name
 * @property string email
 * @property string phone
 * @property string document
 * @property string picture
 * @property integer status_client_id
  * @property integer enterprise_id
 */
class Client extends Model
{
    use SoftDeletes;

    public $table = 'clients';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'email',
        'phone',
        'document',
        'picture',
        'status_client_id',
        'enterprise_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'document' => 'string',
        'picture' => 'string',
        'status_client_id' => 'integer',
        'enterprise_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required',
        'phone' => 'required',
        'document' => 'required',
        'picture' => 'required',
        'password' => 'required|string|min:6|confirmed',//para cadastro simultâneo do user no ClientController
    ];

    public static $rules_update = [
        'name' => 'required',
        'email' => 'required',
        'phone' => 'required',
        'document' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function statusClient()
    {
        return $this->belongsTo(\App\Models\StatusClient::class, 'status_client_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function enterprise()
    {
        return $this->belongsTo(\App\Models\Enterprise::class, 'enterprise_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function tags()
    {
        return $this->hasMany(\App\Models\Tag::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function users()
    {
        return $this->hasMany(\App\Models\User::class, 'client_id');
    }
}
