<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TagType
 * @package App\Models
 * @version August 30, 2017, 5:10 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection Tag
 * @property string name
 * @property string description
 * @property string picture
 */
class TagType extends Model
{
    use SoftDeletes;

    public $table = 'tag_types';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'id',
        'name',
        'description',
        'picture',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'description' => 'string',
        'picture' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'description' => 'required',
        'picture' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function tags()
    {
        return $this->hasMany(\App\Models\Tag::class, 'tag_type_id');
    }
}
