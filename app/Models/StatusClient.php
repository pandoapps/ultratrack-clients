<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class StatusClient
 * @package App\Models
 * @version August 30, 2017, 5:24 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection Client
 * @property string name
 * @property string description
 */
class StatusClient extends Model
{
    use SoftDeletes;

    public $table = 'status_clients';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'description',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'description' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function clients()
    {
        return $this->hasMany(\App\Models\Client::class, 'status_client_id');
    }
}
