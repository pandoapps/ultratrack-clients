<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Ct
 * @package App\Models
 * @version August 30, 2017, 5:49 pm UTC
 *
 * @property integer tag_id
 * @property integer config_id
 * @property string value
 */
class Ct extends Model
{

    public $table = 'cts';

    public $fillable = [
        'tag_id',
        'config_id',
        'value'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'tag_id' => 'integer',
        'config_id' => 'integer',
        'value' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'value' => 'required'
    ];

    
}
