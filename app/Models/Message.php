<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Message
 * @package App\Models
 * @version August 30, 2017, 5:07 pm UTC
 *
 * @property \App\Models\Enterprise enterprise
 * @property string title
 * @property string description
 * @property boolean is_new
 */
class Message extends Model
{
    use SoftDeletes;

    public $table = 'messages';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'id',
        'title',
        'description',
        'is_new',
        'enterprise_id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'description' => 'string',
        'is_new' => 'boolean',
        'enterprise_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        'description' => 'required',
        'is_new' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function enterprise()
    {
        return $this->belongsTo(\App\Models\Enterprise::class, 'enterprise_id', 'id');
    }
}
