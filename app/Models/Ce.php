<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Ce
 * @package App\Models
 * @version August 30, 2017, 5:48 pm UTC
 *
 * @property integer enterprise_id
 * @property integer config_id
 * @property string value
 */
class Ce extends Model
{

    public $table = 'ces';

    public $fillable = [
        'enterprise_id',
        'config_id',
        'value'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'enterprise_id' => 'integer',
        'config_id' => 'integer',
        'value' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'value' => 'required'
    ];

    
}
