<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class StatusTag
 * @package App\Models
 * @version August 31, 2017, 1:09 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection Tag
 * @property string name
 * @property string description
 */
class StatusTag extends Model
{
    use SoftDeletes;

    public $table = 'status_tags';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'id',
        'name',
        'description',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'description' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function tags()
    {
        return $this->hasMany(\App\Models\Tag::class, 'status_tag_id');
    }
}
