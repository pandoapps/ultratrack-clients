<?php

namespace App\Http\Controllers;

use App\DataTables\EnterpriseDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateEnterpriseRequest;
use App\Http\Requests\UpdateEnterpriseRequest;
use App\Repositories\EnterpriseRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;

class EnterpriseController extends AppBaseController
{
    /** @var  EnterpriseRepository */
    private $enterpriseRepository;

    public function __construct(EnterpriseRepository $enterpriseRepo)
    {
        $this->enterpriseRepository = $enterpriseRepo;
    }

    /**
     * Display a listing of the Enterprise.
     *
     * @param EnterpriseDataTable $enterpriseDataTable
     * @return Response
     */
    public function index()
    {
        return redirect(route('enterprises.show', Auth::user()->enterprise_id));
    }

    /**
     * Display the specified Enterprise.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {               
        $enterprise = $this->enterpriseRepository->findWithoutFail($id);

        if (empty($enterprise)) {
            Flash::error('Empresa não encontrada');

            return redirect(route('enterprises.index'));
        }

        return view('enterprises.show')->with('enterprise', $enterprise);
    }

}
