<?php

namespace App\Http\Controllers;

use App\DataTables\TagTypeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTagTypeRequest;
use App\Http\Requests\UpdateTagTypeRequest;
use App\Repositories\TagTypeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class TagTypeController extends AppBaseController
{
    /** @var  TagTypeRepository */
    private $tagTypeRepository;

    public function __construct(TagTypeRepository $tagTypeRepo)
    {
        $this->tagTypeRepository = $tagTypeRepo;
    }

    /**
     * Display a listing of the TagType.
     *
     * @param TagTypeDataTable $tagTypeDataTable
     * @return Response
     */
    public function index(TagTypeDataTable $tagTypeDataTable)
    {
        return $tagTypeDataTable->render('tag_types.index');
    }

    /**
     * Show the form for creating a new TagType.
     *
     * @return Response
     */
    public function create()
    {
        return view('tag_types.create');
    }

    /**
     * Store a newly created TagType in storage.
     *
     * @param CreateTagTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateTagTypeRequest $request)
    {
        $input = $request->all();

        $tagType = $this->tagTypeRepository->create($input);

        Flash::success('Tag Type saved successfully.');

        return redirect(route('tagTypes.index'));
    }

    /**
     * Display the specified TagType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tagType = $this->tagTypeRepository->findWithoutFail($id);

        if (empty($tagType)) {
            Flash::error('Tag Type not found');

            return redirect(route('tagTypes.index'));
        }

        return view('tag_types.show')->with('tagType', $tagType);
    }

    /**
     * Show the form for editing the specified TagType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tagType = $this->tagTypeRepository->findWithoutFail($id);

        if (empty($tagType)) {
            Flash::error('Tag Type not found');

            return redirect(route('tagTypes.index'));
        }

        return view('tag_types.edit')->with('tagType', $tagType);
    }

    /**
     * Update the specified TagType in storage.
     *
     * @param  int $id
     * @param UpdateTagTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTagTypeRequest $request)
    {
        $tagType = $this->tagTypeRepository->findWithoutFail($id);

        if (empty($tagType)) {
            Flash::error('Tag Type not found');

            return redirect(route('tagTypes.index'));
        }

        $tagType = $this->tagTypeRepository->update($request->all(), $id);

        Flash::success('Tag Type updated successfully.');

        return redirect(route('tagTypes.index'));
    }

    /**
     * Remove the specified TagType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tagType = $this->tagTypeRepository->findWithoutFail($id);

        if (empty($tagType)) {
            Flash::error('Tag Type not found');

            return redirect(route('tagTypes.index'));
        }

        $this->tagTypeRepository->delete($id);

        Flash::success('Tag Type deleted successfully.');

        return redirect(route('tagTypes.index'));
    }
}
