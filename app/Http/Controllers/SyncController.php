<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController;
use App\Exceptions\Handler;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Http\Response;
use Flash;
use DB;

use App\Models\StatusEnterprise;
use App\Models\User;
use App\Models\StatusClient;
use App\Models\StatusTag;
use App\Models\Enterprise;
use App\Models\Role;
use App\Models\Tag;
use App\Models\Message;
use App\Models\Config;
use App\Models\TagType;
use App\Models\Ct;
use App\Models\Ce;

class SyncController extends AppBaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    /*
        Não recebe listens e nem event_actions.

        Ao implementar alguma nova funcionalidade de insert em alguma tabela, verificar se vai dar algum conflito 
        do valor de id setado pelo nextval com id que é recebido pelo sync. Exemplo: crio um usuário de id=2 e, 
        posteriormente, recebo um usuário de id=2.

        As tabelas ces e cts não usam softdelete, logo, se for excluído algum dado dessas tabelas não tem como no
        sync saber se deve deletar algum dado. Assim, por hora o sync só está inserindo ou atualizando os dados dessas
        tabelas.

        retorna 200 quando a requisição estava ok, mas não inseriu no banco então na msg descreve-se o erro;
        retorna 201 quando a requisição estava ok e realmente inseriu no banco.

    */

    public function sync_db(Request $request){

        DB::beginTransaction();

        //recebe um body com formato raw e o json em formato de texto
        if(!$request->getContent()){ 
            return response('Esperava-se receber um JSON.', 400);
        }
         
        $json_str = json_decode($request->getContent(), true);

        if ($json_str['enterprise']['field_key'] == env('FIELD_KEY')){ //verifica se chave é correta
    
            /*
                status_enterprises  
            */
    
            $status_enterprises = $json_str['enterprise']['status_enterprises'];
    
            $maior=0;
    
            foreach($status_enterprises as $status_enterprise){ 
    
                $status_enterprise_db = StatusEnterprise::updateOrCreate(
                    ['id' => $status_enterprise['id']],         //verifica pelo id 
                    [
                        'name' => $status_enterprise['name'],
                        'description' => $status_enterprise['description'],
                        'created_at' => $status_enterprise['created_at'],
                        'updated_at' => $status_enterprise['updated_at'],
                        'deleted_at' => $status_enterprise['deleted_at']
                    ]
                );
                                  
                if(!$status_enterprise_db){
                    DB::rollBack();
                    return response('Um erro ocorreu ao inserir ou atualizar o status_enterprise.', 200);
                }
    
                $id = $status_enterprise['id'];
                        
                if($id > $maior){
                    $maior = $id; //para atualização do nextval
                }
            }
    
            /* é necessário atualizar next_val da tabela pois quando houver uma inserção manual sem especificar
               o id, o next_val iniciará em 1 e ocorrerá um conflito de valor de pk */
    
            //se $maior for igual a 0, quer dizer que nenhum dado foi inserido naquela tabela e não é necessário atualizar o nextval
            if($maior!=0){ 
                $next_id = DB::select("select setval('status_enterprises_id_seq', ".$maior.")");
            }
                            
            if((!$next_id) && ($maior!=0)){
                DB::rollBack();
                return response('Um erro ocorreu ao setar o nextval da status_enterprises.', 200);
            }          
                
            /*
                enterprise - apenas uma
            */
    
            $enterprise = $json_str['enterprise'];
    
            $enterprise_db = Enterprise::updateOrCreate(
                ['id' => $enterprise['id']],
                [
                    'name' => $enterprise['name'],       
                    'email' => $enterprise['email'],
                    'phone' => $enterprise['phone'],
                    'document' => $enterprise['document'],
                    'logo' => $enterprise['logo'],
                    'field_key' => $enterprise['field_key'],
                    'endpoint' => $enterprise['endpoint'],
                    'status_enterprise_id' => $enterprise['status_enterprise_id'],
                    'created_at' => $enterprise['created_at'],
                    'updated_at' => $enterprise['updated_at'],
                    'deleted_at' => $enterprise['deleted_at']
                ]
            );
               
            if(!$enterprise_db){
                DB::rollBack();
                return response('Um erro ocorreu ao inserir ou atualizar a enterprise.', 200);
            }            
                    
            /*
                messages
            */
    
            $messages = $json_str['enterprise']['messages'];
    
            $maior=0;
                
            foreach($messages as $message){ 
    
                $message_db = Message::updateOrCreate(
                    ['id' => $message['id']],
                    [
                        'title' => $message['title'],
                        'description' => $message['description'],
                        'is_new' => $message['is_new'],
                        'enterprise_id' => $message['enterprise_id'],
                        'created_at' => $message['created_at'],
                        'updated_at' => $message['updated_at'],
                        'deleted_at' => $message['deleted_at']
                    ]
                );
                    
                if(!$message_db){
                    DB::rollBack();
                    return response('Um erro ocorreu ao inserir ou atualizar a message.', 200);
                }
                
                $id = $message['id'];
                
                if($id > $maior){
                    $maior = $id;
                }            
            }
    
            if($maior!=0){ 
                $next_id = DB::select("select setval('messages_id_seq', ".$maior.")");
            }
                            
            if((!$next_id) && ($maior!=0)){
                DB::rollBack();
                return response('Um erro ocorreu ao setar o nextval da messages.', 200);
            } 
            
            /*
                configs
            */
    
            $configs = $json_str['configs_default'];
            
                $maior=0;
                        
                foreach($configs as $config){ 
            
                    $config_db = Config::updateOrCreate(
                        ['id' => $config['id']],
                        [
                            'name' => $config['name'],
                            'description' => $config['description'],
                            'is_active' => $config['is_active'],
                            'value' => $config['value'],
                            'created_at' => $config['created_at'],
                            'updated_at' => $config['updated_at'],
                            'deleted_at' => $config['deleted_at']
                        ]
                    );
                            
                    if(!$config_db){
                        DB::rollBack();
                        return response('Um erro ocorreu ao inserir ou atualizar a config.', 200);
                    } 
    
                    $id_config = $config['id'];

                    if($id_config > $maior){
                        $maior = $id_config; 
                    }
        
                }
            
                if($maior!=0){ 
                    $next_id_config = DB::select("select setval('configs_id_seq', ".$maior.")");
                }
                                    
                if((!$next_id) && ($maior!=0)){
                    DB::rollBack();
                    return response('Um erro ocorreu ao setar o nextval da config.', 200);
                } 
                
            /*
                tag_types
            */
    
            $tag_types = $json_str['enterprise']['tag_types'];
    
            $maior=0;
                            
            foreach($tag_types as $tag_type){ 
    
                $tag_type_db = TagType::updateOrCreate(
                    ['id' => $tag_type['id']],
                    [
                        'name' => $tag_type['name'],
                        'description' => $tag_type['description'],
                        'picture' => $tag_type['picture'],
                        'created_at' => $tag_type['created_at'],
                        'updated_at' => $tag_type['updated_at'],
                        'deleted_at' => $tag_type['deleted_at']
                    ]
                );
                                
                if(!$tag_type_db){
                    DB::rollBack();
                    return response('Um erro ocorreu ao inserir ou atualizar o tag_type.', 200);
                }
                            
                $id = $tag_type['id'];
                            
                if($id > $maior){
                    $maior = $id;
                }            
            }
    
            if($maior!=0){ 
                $next_id = DB::select("select setval('tag_types_id_seq', ".$maior.")");
            }
                            
            if((!$next_id) && ($maior!=0)){
                DB::rollBack();
                return response('Um erro ocorreu ao setar o nextval da tag_types.', 200);
            }  
                
            /*
                status_tags
            */
    
            $status_tags = $json_str['enterprise']['status_tags'];
                
            $maior=0;
                                        
            foreach($status_tags as $status_tag){ 
    
                $status_tag_db = StatusTag::updateOrCreate(
                    ['id' => $status_tag['id']],
                    [
                        'name' => $status_tag ['name'],
                        'description' => $status_tag ['description'],
                        'created_at' => $status_tag ['created_at'],
                        'updated_at' => $status_tag ['updated_at'],
                        'deleted_at' => $status_tag ['deleted_at']
                    ]
                );
                                            
                if(!$status_tag_db){
                    DB::rollBack();
                    return response('Um erro ocorreu ao inserir ou atualizar o status_tag.', 200);
                }
                                        
                $id = $status_tag['id'];
                                                                            
                if($id > $maior){
                    $maior = $id; 
                }            
            }
    
            if($maior!=0){ 
                $next_id = DB::select("select setval('status_tags_id_seq', ".$maior.")");
            }
                            
            if((!$next_id) && ($maior!=0)){
                DB::rollBack();
                return response('Um erro ocorreu ao setar o nextval da status_tags.', 200);
            }
                
            /*
                tags e cts
            */
    
            $tags = $json_str['enterprise']['tags'];
                
            $maior_idtag=0;
                            
            foreach($tags as $tag){
                                        
                //não atualiza ou insere o last_point e degrees pois vai receber atualizações via post
                $tag_db = Tag::updateOrCreate(
                    ['id' => $tag['id']],
                    [
                        'name' => $tag['name'],
                        'description' => $tag['description'],
                        'enterprise_id' => $tag['enterprise_id'],
                        'status_tag_id' => $tag['status_tag_id'],
                        'tag_type_id' => $tag['tag_type_id'],
                        'created_at' => $tag['created_at'],
                        'updated_at' => $tag['updated_at'],
                        'deleted_at' => $tag['deleted_at']
                    ]
                ); 
    
                if(!$tag_db){
                    DB::rollBack();
                    return response('Um erro ocorreu ao inserir a tag.', 200);
                }
                            
                $id_tag = $tag['id'];
                            
                if($id_tag > $maior_idtag){
                    $maior_idtag = $id_tag; 
                }              
    
                foreach($tag['configs'] as $config){ //configs e cts
    
                    /* Laravel até a versão 5.4 não permite setar mais de uma chave primária no model, então não é possível usar, 
                       no caso de tabelas pivot, o updateOrCreate */
    
                    $tag_db = Tag::find($config['pivot']['tag_id']);
                    $cts = $tag_db->configs->contains($config['pivot']['config_id']);
       
                    if(!$cts){ //insert
                        /* uso do try catch pois o attach não retorna nenhum parâmetro de forma a possibilitar
                           a checagem de se a inserção foi realizada com sucesso */
                        try{ 
       
                            $tag_db->configs()->attach($config['pivot']['config_id'],['value'=>$config['pivot']['value']]);

                        } catch (Exception $e){
       
                            DB::rollBack();
                            return response('Um erro ocorreu ao inserir a cts.', 200);
                        }
                    }
                    else{ //update  
                                    
                        $cts_db = $tag_db->configs()->updateExistingPivot($config['pivot']['config_id'],
                                  ['value' => $config['pivot']['value']]);
                                                         
                        if(!$cts_db){
                            DB::rollBack();
                            return response('Um erro ocorreu ao atualizar a cts.', 200);
                        }                
                    }
                }
            }
    
            if($maior_idtag!=0){ 
                $next_id_tag = DB::select("select setval('tags_id_seq', ".$maior_idtag.")");
            }
                            
            if(!$next_id_tag && $maior_idtag!=0){
                DB::rollBack();
                return response('Um erro ocorreu ao setar o nextval da tags.', 200);
            }           
                
            /*
                ces
            */
    
            $configs = $json_str['enterprise']['configs'];
                            
            foreach($configs as $config){   
                 
                $enterprise_db = Enterprise::find($config['pivot']['enterprise_id']);
                $ces = $enterprise_db->configs->contains($config['pivot']['config_id']);
    
                if(!$ces){ //insert
    
                    try{ 
    
                        $enterprise_db->configs()->attach($config['pivot']['config_id'],['value'=>$config['pivot']['value']]);
    
                    } catch (Exception $e){
    
                        DB::rollBack();
                        return response('Um erro ocorreu ao inserir a ces.', 200);
                    }
                }
                else{ //update  
                                 
                    $ces_db = $enterprise_db->configs()->updateExistingPivot
                              ($config['pivot']['config_id'], ['value' => $config['pivot']['value']]);
                                                      
                    if(!$ces_db){
                        DB::rollBack();
                        return response('Um erro ocorreu ao atualizar a ces.', 200);
                    }                
                }
            }
    
            /*
                users -- apenas um user
            */
    
            $user = $json_str['enterprise']['users'];
    
            $user_db = User::firstOrCreate(
                ['email' => $user['email']],
                [
                    'name' => $user['name'],
                    'phone' => $user['phone'],
                    'picture' => $user['picture'],
                    'enterprise_id' => $user['enterprise_id'],
                    'password' => bcrypt(env('PASSWORD_USER_DEFAULT')),
                    'created_at' => $user['created_at'],
                    'updated_at' => $user['updated_at']                       
                ]
            );
                                            
            if(!$user_db){
                DB::rollBack();
                return response('Um erro ocorreu ao inserir o user.', 200);
            }

            $user_db = User::where('email','=', $user['email'])->first();
    
            $next_id = DB::select("select setval('users_id_seq', ".$user_db->id.")");
                                        
            if(!$next_id){
                DB::rollBack();
                return response('Um erro ocorreu ao setar o nextval de users.', 200);
            }
    
            /*
                roles -- apenas client e enterprise
            */
    
            $role_db = Role::firstOrCreate(
                [
                    'id' => 1,
                    'name' => 'client'
                ],
                [
                    'display_name' => 'Cliente',
                    'description' => 'Cliente',
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")                       
                ]
            );
    
            if(!$role_db){
                DB::rollBack();
                return response('Um erro ocorreu ao inserir a role client.', 200);
            }
    
            $role_db = Role::firstOrCreate(
                [ 
                    'id' => 2,
                    'name' => 'enterprise'
                ],
                [
                    'display_name' => 'Empresa',
                    'description' => 'Empresa',
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")                       
                ]
            );
    
            if(!$role_db){
                DB::rollBack();
                return response('Um erro ocorreu ao inserir a role enterprise.', 200);
            }
    
            /*
                atribui role enterprise ao user
            */
    
            $role_user = $user_db->roles->contains(2);
    
            if(!$role_user){ //insert
    
                try{ 
    
                    $user_db->roles()->attach(2);
    
                } catch (Exception $e){
    
                    DB::rollBack();
                    return response('Um erro ocorreu ao inserir a role_user.', 200);
                }
            }

            /*
                commit no banco de dados
            */
    
            DB::commit();
        }
        else{
    
            return response('Chave incorreta. Operação negada.', 400);    
        }

        return response('Sincronização concluída com sucesso.', 201);
    }

}