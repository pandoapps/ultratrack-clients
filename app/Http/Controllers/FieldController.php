<?php

namespace App\Http\Controllers;

use App\DataTables\FieldDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateFieldRequest;
use App\Http\Requests\UpdateFieldRequest;
use App\Repositories\FieldRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Models\Tag;
use App\Models\Enterprise;
use DB;
use Auth;

class FieldController extends AppBaseController
{
    /** @var  FieldRepository */
    private $fieldRepository;

    public function __construct(FieldRepository $fieldRepo)
    {
        $this->fieldRepository = $fieldRepo;
    }

    /**
     * Display a listing of the Field.
     *
     * @param FieldDataTable $fieldDataTable
     * @return Response
     */
    public function index(FieldDataTable $fieldDataTable)
    {
        return $fieldDataTable->render('fields.index');
    }

    /**
     * Show the form for creating a new Field.
     *
     * @return Response
     */
    public function create()
    {
        $id_enterprise =  Auth::user()->enterprise_id;        
        $enterprise = Enterprise::where('id', $id_enterprise)->first();
        $name_enterprise = $enterprise->name;

        return view('fields.create', compact('name_enterprise','id_enterprise'));
    }

    /**
     * Store a newly created Field in storage.
     *
     * @param CreateFieldRequest $request
     *
     * @return Response
     */
    public function store(CreateFieldRequest $request)
    {
        $input = $request->all();

        $field = $this->fieldRepository->create($input);

        $tags = Tag::where('enterprise_id', $field->enterprise_id)->get();
        foreach($tags as $tag){
            $tag->fields()->attach($field->id, ['value' => "-"]);
        }

        Flash::success('Campo salvo com sucesso.');

        return redirect(route('fields.index'));
    }

    /**
     * Display the specified Field.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $field = $this->fieldRepository->findWithoutFail($id);
        if (empty($field)) {
            Flash::error('Campo não encontrado.');

            return redirect(route('fields.index'));
        } 

        $id_enterprise =  Auth::user()->enterprise_id;        
        $enterprise = Enterprise::where('id', $id_enterprise)->first();
        $name_enterprise = $enterprise->name;

        return view('fields.show', compact('name_enterprise'))->with('field', $field);
    }

    /**
     * Show the form for editing the specified Field.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
       $field = $this->fieldRepository->findWithoutFail($id);
       
        if (empty($field)) {
            Flash::error('Campo não encontrado.');

            return redirect(route('fields.index'));
        }

        $id_enterprise =  Auth::user()->enterprise_id;        
        $enterprise = Enterprise::where('id', $id_enterprise)->first();
        $name_enterprise = $enterprise->name;

        return view('fields.edit', compact('name_enterprise','id_enterprise'))->with('field', $field);
    }

    /**
     * Update the specified Field in storage.
     *
     * @param  int $id
     * @param UpdateFieldRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFieldRequest $request)
    {
        $field = $this->fieldRepository->findWithoutFail($id);

        if (empty($field)) {
            Flash::error('Campo não encontrado.');

            return redirect(route('fields.index'));
        }

        $field = $this->fieldRepository->update($request->all(), $id);

        Flash::success('Campo atualizado com sucesso.');

        return redirect(route('fields.index'));
    }

    /**
     * Remove the specified Field from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $field = $this->fieldRepository->findWithoutFail($id);

        if (empty($field)) {
            Flash::error('Campo não encontrado.');

            return redirect(route('fields.index'));
        }

        $tags = Tag::where('enterprise_id', $field->enterprise_id)->get();
        foreach($tags as $tag){
            $tag->fields()->detach($field->id);
        }

        $this->fieldRepository->delete($id);

        Flash::success('Campo deletado com sucesso.');

        return redirect(route('fields.index'));
    }
    
}
