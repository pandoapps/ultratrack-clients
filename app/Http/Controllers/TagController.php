<?php

namespace App\Http\Controllers;

use App\DataTables\TagDataTable;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateTagRequest;
use App\Repositories\TagRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use App\Models\StatusTag;
use App\Models\Tag;
use App\Models\Field;
use App\Models\TagType;
use App\Models\Client;
use App\Models\Enterprise;
use DB;

class TagController extends AppBaseController
{
    /** @var  TagRepository */
    private $tagRepository;

    public function __construct(TagRepository $tagRepo)
    {
        $this->tagRepository = $tagRepo;
    }

    /**
     * Display a listing of the Tag.
     *
     * @param TagDataTable $tagDataTable
     * @return Response
     */
    public function index(TagDataTable $tagDataTable)
    {       
        return $tagDataTable->render('tags.index');
    }

    /**
     *
     * Função usada para exibição da localização das tags
     *
     */
    public function tag_tracker(Request $request){
        $tag = Tag::find($request['tag_id']);        
        $fromDate = $request['start_date'];
        $toDate = $request['end_date'];
        $query = DB::select(
            "SELECT ST_AsGeoJSON(last_point)
             from listens 
             where tag_id = " . $tag->id . " 
             and listened_at between '" . $fromDate . "'             
             and '" . $toDate . "' ORDER BY id ASC"
            
        );  
        $array = []; 
        foreach($query as $i=>$listen){
            $array[$i] = json_decode($listen->st_asgeojson)->coordinates; 
        }
        
        return response()->json(['items' => $array]);
    }

    /**
     * Display the specified Tag.
     *
     * @param  int $id
     *
     * @return Response
     */
     
    public function show($id)
    {
        $tag = $this->tagRepository->findWithoutFail($id);

        if (empty($tag)) {
            Flash::error('Tag não encontrada.');

            return redirect(route('tags.index'));
            
        }

        //recupero os valores dos campos que são fk
        $enterprise = Enterprise::where('id', $tag->enterprise_id)->first();
        $name_enterprise = $enterprise->name;

       $client = Client::where('id','=',$tag->client_id)->first();
       $name_client = $client->name;

       $status = StatusTag::where('id','=',$tag->status_tag_id)->first();
       $status_tags = $status->name;

       $type = TagType::where('id','=',$tag->tag_type_id)->first();
       $tag_type = $type->name;

       $fields = $tag->fields; //fields vinculados à tag

       return view('tags.show', compact('name_enterprise', 'name_client', 'status_tags','tag_type',
       'fields'))->with('tag', $tag);
    }

    /**
     * Show the form for editing the specified Tag.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tag = $this->tagRepository->findWithoutFail($id);

        if (empty($tag)) {
            Flash::error('Tag não encontrada.');

            return redirect(route('tags.index'));
        }
        
       //recupero os valores dos campos que são fk
       $clients = Client::orderBy('name','asc')->pluck('name','id')->all();
       $status_tags = StatusTag::orderBy('name','asc')->pluck('name','id')->all();

       $id_enterprise = $tag->enterprise_id;
       $enterprise = Enterprise::where('id', $id_enterprise)->first();
       $name_enterprise = $enterprise->name;

       $fields = $tag->fields; //fields vinculados à tag

        return view('tags.edit', compact('status_tags','clients','name_enterprise','fields',
        'id_enterprise'))->with('tag', $tag);
    }

    /**
     * Update the specified Tag in storage.
     *
     * @param  int $id
     * @param UpdateTagRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTagRequest $request)
    {
        $tag = $this->tagRepository->findWithoutFail($id);

        if (empty($tag)) {
            Flash::error('Tag não encontrada.');

            return redirect(route('tags.index'));
        }

        if (Auth::user()->hasRole('client')){ //usuário do tipo client, tem menos campos
            $tag->name = $request->name;
            $tag->description = $request->description;
            $tag->save();  

            //salvo na tag_fields
            $fields = $tag->fields;
            $i=0;
            foreach ($fields as $field){
                $field->pivot->value=$request->array[$i];
                $field->pivot->save();
                $i++;
            }
        }
        else if (Auth::user()->hasRole('enterprise')){//usuário do tipo enterprise
            $tag->name = $request->name;
            $tag->description = $request->description;
            $tag->client_id = $request->client_id;
            $tag->status_tag_id = $request->status_tag_id;
            $tag->tag_type_id = $request->tag_type_id;
            $tag->enterprise_id = $request->enterprise_id;
            $tag->save();

            //salvo na tag_fields
            $fields = $tag->fields;
            $i=0;
            foreach ($fields as $field){
                $field->pivot->value=$request->array[$i];
                $field->pivot->save();
                $i++;
            }

        }
        
        Flash::success('Dados da tag atualizados com sucesso.');

        return redirect(route('tags.index'));
    }

    /**
     * Remove the specified Tag from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tag = $this->tagRepository->findWithoutFail($id);

        if (empty($tag)) {
            Flash::error('Tag não encontrada.');

            return redirect(route('tags.index'));
        }

        $this->tagRepository->delete($id);

        Flash::success('Tag deletada com sucesso.');

        return redirect(route('tags.index'));
    }
}