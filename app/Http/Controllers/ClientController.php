<?php

namespace App\Http\Controllers;

use App\DataTables\ClientDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateClientRequest;
use App\Http\Requests\UpdateClientRequest;
use App\Repositories\ClientRepository;
use App\Exceptions\Handler;
use App\Http\Controllers\AppBaseController;
use Flash;
use Response;
use Auth;
use DB;
use File;
use Storage;
use App\Models\User;
use App\Models\Tag;
use App\Models\StatusClient;
use App\Models\Enterprise;
use App\Models\Role;

class ClientController extends AppBaseController
{
    /** @var  ClientRepository */
    private $clientRepository;

    public function __construct(ClientRepository $clientRepo)
    {
        $this->clientRepository = $clientRepo;
    }

    /**
     * Display a listing of the Client.
     *
     * @param ClientDataTable $clientDataTable
     * @return Response
     */
    public function index(ClientDataTable $clientDataTable)
    {
        return $clientDataTable->render('clients.index');
    }

    /**
     * Show the form for creating a new Client.
     *
     * @return Response
     */
    public function create()
    { //função acionada por qualquer pessoa   
        $status=StatusClient::pluck('name','id')->all();
        $id_enterprise = DB::select('select id from enterprises');//apenas uma empresa estará cadastrada
        $enterprise_id=$id_enterprise[0]->id;

        return view('clients.create', compact('status','enterprise_id'));

    }

    /**
     * Store a newly created Client in storage.
     *
     * @param CreateClientRequest $request
     *
     * @return Response
     */
    public function store(CreateClientRequest $request)
    {

        $input = $request->all();

        DB::transaction(function () use ($input,$request) {

            $client = $this->clientRepository->create($input);

            //cadastro de usuário
            $status=StatusClient::pluck('name','id')->all();
            $enterprise_id = $client->enterprise_id;

            if (!$client){ //não criou o cliente
                
                Flash::error('Cliente não foi criado.');
                return view('clients.create', compact('status','enterprise_id'));
            }
            else{
                if (!$request->file('picture')){ //não foi enviada a imagem

                    Flash::error('Insira uma foto.');
                    return view('clients.create', compact('status','enterprise_id'));
                }
                else{ 

                    // ** não esquecer de fazer antes um link simbólico de public/storage para storage/app/public
                    //no laravel é feito pelo comando php artisan storage:link
                    
                    //inicialmene imagem do usuário será igual a imagem do cliente
                    $path_client = $request->file('picture')->store('public/pictures/clients');
                    $path_user = $request->file('picture')->store('public/pictures/users');
                
                    $url_picture_client = explode("/", $path_client); //objetivo de eliminar o public da url
                    $url_picture_user = explode("/", $path_user); //objetivo de eliminar o public da url
                
                    if ($path_client && $path_user){ //criou o arquivo
                
                        $client->picture = "pictures/clients/".$url_picture_client[3];
                        $picture_user = "pictures/users/".$url_picture_user[3];
                        $client->save();
                
                    }
                    else{
                
                        Flash::error('Não foi possível salvar a foto.');      
                        return view('clients.create', compact('status','enterprise_id'));              
                    }
                }
            }

            $user = User::create([
                'name' => $client->name,
                'email' => $client->email,
                'password' => bcrypt($request->password),
                'enterprise_id' => $enterprise_id,
                'client_id' => $client->id,
                'phone' => $client->phone,
                'picture' => $picture_user,
            ]);

            if (!$user){
                
                Flash::error('Erro ao salvar os dados do cliente.');      
                return view('clients.create', compact('status','enterprise_id')); 
            }

            $client_role = Role::where('name', '=', 'client')->first();        
            $user->attachRole($client_role); //atribui role 

        }, 1); 

        Flash::success('Cliente salvo com sucesso.');
    
        return redirect('/login');
    }

    /**
     * Display the specified Client.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $client = $this->clientRepository->findWithoutFail($id);

        if (empty($client)) {
            Flash::error('Cliente não encontrado.');

            return redirect(route('clients.index'));
        }

       //recupero os valores dos campos que são fk
       $status=StatusClient::where('id','=',$client->status_client_id)->first();
       $name_status= $status->name;
       
       $enterprise = Enterprise::where('id', $client->enterprise_id)->first();
       $name_enterprise = $enterprise->name;

        return view('clients.show', compact('name_status','name_enterprise'))->with('client', $client);
    }

    /**
     * Show the form for editing the specified Client.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $client = $this->clientRepository->findWithoutFail($id);

        if (empty($client)) {
            Flash::error('Cliente não encontrado.');

            return redirect(route('clients.index'));
        }
        
        $status=StatusClient::pluck('name','id')->all();
        $id_enterprise =  Auth::user()->enterprise_id;   
        return view('clients.edit', compact('status', 'id_enterprise'))->with('client', $client);
    }

    /**
     * Update the specified Client in storage.
     *
     * @param  int $id
     * @param UpdateClientRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClientRequest $request)
    {
        $client = $this->clientRepository->findWithoutFail($id);

        if (empty($client)) {
            Flash::error('Cliente não encontrado.');

            return redirect(route('clients.index'));
        }

        $client = $this->clientRepository->update($request->except('picture'), $id);
        
        if (!$client){ //não atualizou o cliente

            Flash::error('Cliente não foi atualizado.');
        }
        else {
            
            if ($request->file('picture')){ //foi enviada a imagem

                $path = $request->file('picture')->store('public/pictures/clients');

                $url_new_picture = explode("/", $path); //objetivo de eliminar o public da url

                if ($path){ //criou o arquivo

                    //apago a picture antiga
                    $url_old_picture = explode("/", $client->picture);
                    
                    if (Storage::exists('public/pictures/clients/'.$url_old_picture[2])){

                        Storage::delete('public/pictures/clients/'.$url_old_picture[2]); 
                    
                    }                   

                    //salvo no banco de dados a url da nova picture
                    $client->picture = "pictures/clients/".$url_new_picture[3];
                    $client->save();         
                
                }
                else{

                    Flash::error('Não foi possível salvar a foto.');                    
                }
            }
        }

        Flash::success('Cliente atualizado com sucesso.');

        return redirect(route('clients.index'));
    }

    /**
     * Remove the specified Client from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $client = $this->clientRepository->findWithoutFail($id);

        if (empty($client)) {
            Flash::error('Cliente não encontrado.');

            return redirect(route('clients.index'));
        }

        DB::transaction(function () use ($client) {

            try{
                $user_find = User::where('client_id','=',$client->id)->first();

                if($user_find){

                    $user_find->delete();
                    $tags_find = Tag::where('client_id','=',$client->id)->get();

                    foreach($tags_find as $tag_find){

                        $tag_find->client_id = null;
                        $tag_find->save();   
                    }

                }

                $client->delete();

                Flash::success('Cliente deletado com sucesso.');
            }   
            catch(Exception $e){

                Flash::error('Um erro ocorreu durante a exclusão do cliente.');
            }

        }); 

        return redirect(route('clients.index'));
    }
}
