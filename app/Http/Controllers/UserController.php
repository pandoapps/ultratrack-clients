<?php

namespace App\Http\Controllers;

use App\DataTables\UserDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use Storage;
use App\Models\Role;
use App\Models\User;

class UserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param UserDataTable $userDataTable
     * @return Response
     */
    public function index(UserDataTable $userDataTable)
    {
       return redirect(route('users.edit', Auth::user()->id));
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        if (Auth::user()->hasRole('enterprise')){ //cria usuário do tipo enterprise, usuários clientes são criado em clients.store
            $input = $request->all();
            
            $input['enterprise_id'] = Auth::user()->enterprise_id;
            $input['password'] = bcrypt($input['password']);
            $user = $this->userRepository->create($input);

            if (!$user){ //não criou o user
                
                Flash::error('Usuário não foi criado.');
                return view('users.create');
            }
            else{
                if (!$request->file('picture')){ //não foi enviada a imagem

                    Flash::error('Insira uma foto.');
                    return view('users.create');
                }
                else{ 

                    // ** não esquecer de fazer antes um link simbólico de public/storage para storage/app/public
                    //no laravel é feito pelo comando php artisan storage:link
                    
                    $path_user = $request->file('picture')->store('public/pictures/users');
                
                    $url_picture_user = explode("/", $path_user); //objetivo de eliminar o public da url
                
                    if ($path_user){ //criou o arquivo
                
                        $user->picture = "pictures/users/".$url_picture_user[3];
                        $user->save();
                
                    }
                    else{
                
                        Flash::error('Não foi possível salvar a foto.');      
                        return view('users.create');              
                    }
                }
            }

            $enterpise_role = Role::where('name', '=', 'enterprise')->first();               
            $user->attachRole($enterpise_role); //atribui role 
        }

        Flash::success('Usuário salvo com sucesso.');

        return redirect(route('home.index'));
    }

    /**
     * Display the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('Usuário não encontrado');

            return redirect(route('home.index'));
        }

        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('Usuário não encontrado');

            return redirect(route('home.index'));
        }

        return view('users.edit')->with('user', $user);
    }

    /**
     * Update the specified User in storage.
     *
     * @param  int              $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('Usuário não encontrado');

            return redirect(route('home.index'));
        }

        //valida se email está sendo usado por outro usuário
        $valida_user_email= User::where('id','<>', Auth::user()->id)->where('email','=',$request->email)->first();
        
        if ($valida_user_email){
            Flash::error('Este email já está sendo usado por outro usuário.');
            return view('users.edit')->with('user', $user);
        }
        else{ 
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone= $request->phone;

            if (($request->password != '') && (strlen($request->password) >= 6)){
                $user->password = bcrypt($request->password);  
            }  
            else if (($request->password != '') && (strlen($request->password) < 6)){
                Flash::error('A senha tem que possuir no mínimo 6 caracteres.');
                return view('users.edit')->with('user', $user);
            }
    
            $user->save();

            if (!$user){ //não atualizou o user
                
                Flash::error('Dados do usário não foram atualizados.');
            }
            else {
                            
                if ($request->file('picture')){ //foi enviada a imagem
                
                    $path = $request->file('picture')->store('public/pictures/users');
                
                    $url_new_picture = explode("/", $path); //objetivo de eliminar o public da url
                
                    if ($path){ //criou o arquivo
                
                        //apago a picture antiga
                        $url_old_picture = explode("/", $user->picture);

                        if ($user->picture){ //user já tem uma picture

                            if (Storage::exists('public/pictures/users/'.$url_old_picture[2])){

                                    Storage::delete('public/pictures/users/'.$url_old_picture[2]);
                            }
                        }
                
                        //salvo no banco de dados a url da nova picture
                        $user->picture = "pictures/users/".$url_new_picture[3];
                        $user->save();          
             
                    }
                    else{
                
                        Flash::error('Não foi possível salvar a foto.');                    
                    }
                }
            }

            Flash::success('Dados do usuário atualizados com sucesso.');

            return view('users.edit')->with('user', $user);
        }
    }

    /**
     * Remove the specified User from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('Usuário não encontrado');

            return redirect(route('home.index'));
        }

        if (Auth::user()->hasRole('enterprise')){
            //verifica se tem ao menos uma conta do tipo enterprise criada
            $verifica_user_enterprise = User::where('enterprise_id','=', Auth::user()->enterprise_id)->
            where('client_id','=', null)->where('id','<>', Auth::user()->id)->first();

            if (!$verifica_user_enterprise){
                Flash::error('Exclusão não permitida. Deve ao menos ter um usuário do tipo "enterprise" cadastrado.');
                return view('users.edit')->with('user', $user);
            }
        }

        $this->userRepository->delete($id);

        Flash::success('Usuário apagado com sucesso');

        return redirect(route('home.index'));
    }
}