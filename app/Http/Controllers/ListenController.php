<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\UpdateListenRequest;
use App\Repositories\ListenRepository;
use App\Http\Controllers\AppBaseController;
use App\Exceptions\Handler;
use App\Models\Tag;
use App\Models\Listen;
use \ZMQContext;
use \ZMQ;
use Flash;
use DB;

class ListenController extends AppBaseController
{
    /** @var  ListenRepository */
    private $listenRepository;

    public function __construct(ListenRepository $listenRepo)
    {
        $this->listenRepository = $listenRepo;
    }

    /**
     * Display a listing of the Listen.
     *
     * @param ListenDataTable $listenDataTable
     * @return Response
     */
    public function index(ListenDataTable $listenDataTable)
    {
        return $listenDataTable->render('listens.index');
    }

    /**
     * Store a newly created Listen in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
    
        if ($request->field_key == env('FIELD_KEY')){ //verifica se chave é correta

            //validação se todos os campos estão preenchidos
            if (!$request->latitude || !$request->longitude || !$request->speed || !$request->temperature 
                || !$request->degrees || !$request->batery || !$request->tag_id || !$request->timestamp){

                return response('Não foram informados todos os campos e valores necessários.', 400); //por exemplo
            
            }
            else {
                //validação se tag existe
                $tag = Tag::where('id', $request->tag_id)->first();
                if (!$tag){ //id da tag não está cadastrado

                    return response('Tag informada não existe.', 400);

                }
                else{
                    
                    $request['last_point'] = 'SRID=4326;POINT('.$request['latitude'].' '.$request['longitude'].')';//concatena latitude e longitude
                    $input = $request->except(['latitude', 'longitude']); //recebe os dados do request com excessão da latitude e longitude

                    DB::transaction(function () use ($input) {

                        try{

                            $listen = Listen::create([
                                'speed' => $input['speed'],
                                'temperature' => $input['temperature'],
                                'batery' => $input['batery'],
                                'last_point' => $input['last_point'],
                                'tag_id' => $input['tag_id'],
                                'listened_at' => $input['timestamp']/1000,
                            ]); //$input['timestamp']/1000, pois o app PandaPost envia o timestamp 1000 vezes maior

                            $tag = Tag::find($input['tag_id']);            
                            $tag->last_point = $input['last_point'];
                            $tag->degrees = $input['degrees'];
                            $tag->save();
                        }
                        catch(Exception $e){

                            return response('Ocorreu um erro e o listen não foi cadastrado.', 200);  
                        }
                    }); 

                    // Websocket stuff

                    $tag = Tag::find($input['tag_id']);
                    //Cria o contexto do socket
                    $context = new ZMQContext();
                    //Cria o canal 'my-pusher' pro socket
                    $socket = $context->getSocket(ZMQ::SOCKET_PUSH, 'my pusher');
                    //Abre o socket na camada tcp, no ip localhost, na porta 5555
                    $socket->connect("tcp://".env('ADDRESS_ZMQ'));
                    //Envia a mensagem pra quem estiver escutando
                    //passa timestamp igual a -999 para diferenciar de quando for um post de event_action no qual é passado o timestamp real
                    $socket->send(json_encode(['tag'=>$tag, 'lat'=>$request['latitude'], 'lng'=>$request['longitude'], 'timestamp'=>'-999']));

                }
            }

            return response('Listen cadastrado com sucesso.', 201);
        }
        else{
            
            return response('Chave incorreta. Operação negada.', 400);  
        }
    }

    /**
     * Display the specified Listen.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $listen = $this->listenRepository->findWithoutFail($id);

        if (empty($listen)) {
            Flash::error('Listen não encontrado');

            return redirect(route('listens.index'));
        }

        return view('listens.show')->with('listen', $listen);
    }
}