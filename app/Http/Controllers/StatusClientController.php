<?php

namespace App\Http\Controllers;

use App\DataTables\StatusClientDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateStatusClientRequest;
use App\Http\Requests\UpdateStatusClientRequest;
use App\Repositories\StatusClientRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class StatusClientController extends AppBaseController
{
    /** @var  StatusClientRepository */
    private $statusClientRepository;

    public function __construct(StatusClientRepository $statusClientRepo)
    {
        $this->statusClientRepository = $statusClientRepo;
    }

    /**
     * Display a listing of the StatusClient.
     *
     * @param StatusClientDataTable $statusClientDataTable
     * @return Response
     */
    public function index(StatusClientDataTable $statusClientDataTable)
    {
        return $statusClientDataTable->render('status_clients.index');
    }

    /**
     * Show the form for creating a new StatusClient.
     *
     * @return Response
     */
    public function create()
    {
        return view('status_clients.create');
    }

    /**
     * Store a newly created StatusClient in storage.
     *
     * @param CreateStatusClientRequest $request
     *
     * @return Response
     */
    public function store(CreateStatusClientRequest $request)
    {
        $input = $request->all();

        $statusClient = $this->statusClientRepository->create($input);

        Flash::success('Status salvo com sucesso.');

        return redirect(route('statusClients.index'));
    }

    /**
     * Display the specified StatusClient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $statusClient = $this->statusClientRepository->findWithoutFail($id);

        if (empty($statusClient)) {
            Flash::error('Status Client não encontrado.');

            return redirect(route('statusClients.index'));
        }

        return view('status_clients.show')->with('statusClient', $statusClient);
    }

    /**
     * Show the form for editing the specified StatusClient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $statusClient = $this->statusClientRepository->findWithoutFail($id);

        if (empty($statusClient)) {
            Flash::error('Status não encontrado.');

            return redirect(route('statusClients.index'));
        }

        return view('status_clients.edit')->with('statusClient', $statusClient);
    }

    /**
     * Update the specified StatusClient in storage.
     *
     * @param  int              $id
     * @param UpdateStatusClientRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStatusClientRequest $request)
    {
        $statusClient = $this->statusClientRepository->findWithoutFail($id);

        if (empty($statusClient)) {
            Flash::error('Status não encontrado');

            return redirect(route('statusClients.index'));
        }

        $statusClient = $this->statusClientRepository->update($request->all(), $id);

        Flash::success('Status atualizado com sucesso.');

        return redirect(route('statusClients.index'));
    }

    /**
     * Remove the specified StatusClient from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $statusClient = $this->statusClientRepository->findWithoutFail($id);

        if (empty($statusClient)) {
            Flash::error('Status não encontrado.');

            return redirect(route('statusClients.index'));
        }

        $this->statusClientRepository->delete($id);

        Flash::success('Statusdeletado com sucesso.');

        return redirect(route('statusClients.index'));
    }
}
