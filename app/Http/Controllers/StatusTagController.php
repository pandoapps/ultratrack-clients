<?php

namespace App\Http\Controllers;

use App\DataTables\StatusTagDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateStatusTagRequest;
use App\Http\Requests\UpdateStatusTagRequest;
use App\Repositories\StatusTagRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class StatusTagController extends AppBaseController
{
    /** @var  StatusTagRepository */
    private $statusTagRepository;

    public function __construct(StatusTagRepository $statusTagRepo)
    {
        $this->statusTagRepository = $statusTagRepo;
    }

    /**
     * Display a listing of the StatusTag.
     *
     * @param StatusTagDataTable $statusTagDataTable
     * @return Response
     */
    public function index(StatusTagDataTable $statusTagDataTable)
    {
        return $statusTagDataTable->render('status_tags.index');
    }

    /**
     * Show the form for creating a new StatusTag.
     *
     * @return Response
     */
    public function create()
    {
        return view('status_tags.create');
    }

    /**
     * Store a newly created StatusTag in storage.
     *
     * @param CreateStatusTagRequest $request
     *
     * @return Response
     */
    public function store(CreateStatusTagRequest $request)
    {
        $input = $request->all();

        $statusTag = $this->statusTagRepository->create($input);

        Flash::success('Status Tag saved successfully.');

        return redirect(route('statusTags.index'));
    }

    /**
     * Display the specified StatusTag.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $statusTag = $this->statusTagRepository->findWithoutFail($id);

        if (empty($statusTag)) {
            Flash::error('Status Tag not found');

            return redirect(route('statusTags.index'));
        }

        return view('status_tags.show')->with('statusTag', $statusTag);
    }

    /**
     * Show the form for editing the specified StatusTag.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $statusTag = $this->statusTagRepository->findWithoutFail($id);

        if (empty($statusTag)) {
            Flash::error('Status Tag not found');

            return redirect(route('statusTags.index'));
        }

        return view('status_tags.edit')->with('statusTag', $statusTag);
    }

    /**
     * Update the specified StatusTag in storage.
     *
     * @param  int              $id
     * @param UpdateStatusTagRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStatusTagRequest $request)
    {
        $statusTag = $this->statusTagRepository->findWithoutFail($id);

        if (empty($statusTag)) {
            Flash::error('Status Tag not found');

            return redirect(route('statusTags.index'));
        }

        $statusTag = $this->statusTagRepository->update($request->all(), $id);

        Flash::success('Status Tag updated successfully.');

        return redirect(route('statusTags.index'));
    }

    /**
     * Remove the specified StatusTag from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $statusTag = $this->statusTagRepository->findWithoutFail($id);

        if (empty($statusTag)) {
            Flash::error('Status Tag not found');

            return redirect(route('statusTags.index'));
        }

        $this->statusTagRepository->delete($id);

        Flash::success('Status Tag deleted successfully.');

        return redirect(route('statusTags.index'));
    }
}
