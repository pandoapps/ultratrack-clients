<?php

namespace App\Http\Controllers;

use App\DataTables\EventActionDataTable;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\CreateEventActionRequest;
use App\Http\Requests\UpdateEventActionRequest;
use App\Repositories\EventActionRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Tag;
use App\Models\EventAction;
use \ZMQContext;
use \ZMQ;
use Flash;

class EventActionController extends AppBaseController
{
    /** @var  EventActionRepository */
    private $eventActionRepository;

    public function __construct(EventActionRepository $eventActionRepo)
    {
        $this->eventActionRepository = $eventActionRepo;
    }

    /**
     * Display a listing of the EventAction.
     *
     * @param EventActionDataTable $eventActionDataTable
     * @return Response
     */
    public function index(EventActionDataTable $eventActionDataTable)
    {
        return $eventActionDataTable->render('event_actions.index');
    }

    /**
     * Store a newly created EventAction in storage.
     *
     * @return Response
    */
    public function store(Request $request)
    {
     
        if ($request->field_key == env('FIELD_KEY')){ //verifica se chave é correta
 
            //validação se todos os campos estão preenchidos
            if (!$request->latitude || !$request->longitude || !$request->tag_id || !$request->timestamp){
 
                return response('Não foram informados todos os campos e valores necessários.', 400); //por exemplo
  
            }
            else {
                //validação se tag existe
                $tag = Tag::where('id', $request->tag_id)->first();
                if (!$tag){ //id da tag não está cadastrado
 
                    return response('Tag informada não existe.', 400);
 
                }
                else{

                    $request['last_point'] = 'SRID=4326;POINT('.$request['latitude'].' '.$request['longitude'].')';//concatena latitude e longitude
                   
                    $event_action = EventAction::create([
                        'last_point' => $request['last_point'],
                        'tag_id' => $request['tag_id'],
                        'listened_at' => $request['timestamp']/1000,
                    ]); //$request['timestamp']/1000, pois o app PandaPost envia o timestamp 1000 vezes maior
                    
                    if(!$event_action){
                        
                        //retorna 200, pois a requisição estava ok, mas não inseriu no banco então na msg descreve-se o erro
                        return response('Ocorreu um erro e o event_action não foi cadastrado.', 200);
                    }

                    $tag = Tag::find($request['tag_id']);
                    //Cria o contexto do socket
                    $context = new ZMQContext();
                    //Cria o canal 'my-pusher' pro socket
                    $socket = $context->getSocket(ZMQ::SOCKET_PUSH, 'my pusher');
                    //Abre o socket na camada tcp, no ip localhost, na porta 5555
                    $socket->connect("tcp://".env('ADDRESS_ZMQ'));
                    //Envia a mensagem pra quem estiver escutando
                    $socket->send(json_encode(['tag'=>$tag, 'lat'=>$request['latitude'], 'lng'=>$request['longitude'], 'timestamp'=>$request['timestamp']]));
                }                
            }
            
            //retorna 201, pois a requisição estava ok e realmente inseriu no banco
            return response('Event_action cadastrada com sucesso.', 201);
        }
        else{
             
            return response('Chave incorreta. Operação negada.', 400);  
        }
    }

    /**
     * Display the specified EventAction.
     *
     * @param  int $id
     *
     * @return Response
     */
     public function show($id)
     {
         $eventAction = $this->eventActionRepository->findWithoutFail($id);
 
         if (empty($eventAction)) {
             Flash::error('Event Action not found');
 
             return redirect(route('eventActions.index'));
         }
 
         return view('event_actions.show')->with('eventAction', $eventAction);
     }
}
