<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Tag;
use App\Models\Enterprise;
use App\Models\EventAction;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth'); //redireciona para tela de login como página inicial
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {

            $enterprise_id = Auth::user()->enterprise_id; 

            if (Auth::user()->hasRole('client')){ //verifica o usuário logado

                $client_id = Auth::user()->client_id;       
                $tags =  Tag::where('client_id', $client_id)->orderBy('id','asc')->get();
                $event_actions = EventAction::join('tags', 'event_actions.tag_id', '=', 'tags.id')
                                ->select('event_actions.id','event_actions.tag_id','event_actions.last_point','event_actions.listened_at','tags.name')
                                ->where('tags.client_id', $client_id)->get();

            } //lista apenas tags e event_actions do cliente logado
            else if (Auth::user()->hasRole('enterprise')){                

                $tags =  Tag::where('enterprise_id', $enterprise_id)->orderBy('id','asc')->get();
                $event_actions = EventAction::join('tags', 'event_actions.tag_id', '=', 'tags.id')
                                ->select('event_actions.id','event_actions.tag_id','event_actions.last_point','event_actions.listened_at','tags.name')
                                ->where('tags.enterprise_id', $enterprise_id)->get();
                              // + INTERVAL'1 month'
            } //lista apenas tags e event_actions da empresa logada
            
            $enterprise_name = Enterprise::where('id', $enterprise_id)->first()->name;

           return view('home', compact('tags', 'event_actions', 'enterprise_name')); //página inicial depois de logado
        }
        else{ 
            return view('home_site'); //página inicial do site
        }
    }

}
