<?php

namespace App\Http\Controllers;

use App\DataTables\ConfigDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateConfigRequest;
use App\Http\Requests\UpdateConfigRequest;
use App\Repositories\ConfigRepository;
use App\Http\Controllers\AppBaseController;
use App\Exceptions\Handler;
use Response;
use Flash;
use DB;

class ConfigController extends AppBaseController
{

    /** @var  ConfigRepository */
    private $configRepository;

    public function __construct(ConfigRepository $configRepo)
    {
        $this->configRepository = $configRepo;
    }

    /**
     * Display a listing of the Config.
     *
     * @param ConfigDataTable $configDataTable
     * @return Response
     */
    public function index(ConfigDataTable $configDataTable)
    {

        return $configDataTable->render('configs.index');
    }

    public function show($id)
    {
        $config = $this->configRepository->findWithoutFail($id);

        if (empty($config)) {
            Flash::error('Config not found');

            return redirect(route('configs.index'));
        }

        return view('configs.show')->with('config', $config);
    }    
}
