<?php

namespace App\Http\Controllers;

use App\DataTables\StatusEnterpriseDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateStatusEnterpriseRequest;
use App\Http\Requests\UpdateStatusEnterpriseRequest;
use App\Repositories\StatusEnterpriseRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class StatusEnterpriseController extends AppBaseController
{
    /** @var  StatusEnterpriseRepository */
    private $statusEnterpriseRepository;

    public function __construct(StatusEnterpriseRepository $statusEnterpriseRepo)
    {
        $this->statusEnterpriseRepository = $statusEnterpriseRepo;
    }

    /**
     * Display a listing of the StatusEnterprise.
     *
     * @param StatusEnterpriseDataTable $statusEnterpriseDataTable
     * @return Response
     */
    public function index(StatusEnterpriseDataTable $statusEnterpriseDataTable)
    {
        return $statusEnterpriseDataTable->render('status_enterprises.index');
    }

    /**
     * Display the specified StatusEnterprise.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $statusEnterprise = $this->statusEnterpriseRepository->findWithoutFail($id);

        if (empty($statusEnterprise)) {
            Flash::error('Status Enterprise not found');

            return redirect(route('statusEnterprises.index'));
        }

        return view('status_enterprises.show')->with('statusEnterprise', $statusEnterprise);
    }
}
