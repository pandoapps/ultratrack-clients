<?php

namespace App\DataTables;

use App\Models\Tag;
use Form;
use DB;
use Yajra\Datatables\Services\DataTable;
use Auth;

class TagDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'tags.datatables_actions')
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        
       if (Auth::user()->hasRole('client')){ //verifica se tem usuário logado
           $client_id = Auth::user()->client_id;       
           $tags = Tag::leftjoin('clients', 'tags.client_id', '=', 'clients.id')
           ->select('tags.id','tags.name as tag_name','tags.description','clients.name')
           ->where('tags.client_id', $client_id);
       } //lista apenas tags do cliente logado
       else if (Auth::user()->hasRole('enterprise')){
           $enterprise_id = Auth::user()->enterprise_id;       
           $tags =  Tag::leftjoin('clients', 'tags.client_id', '=', 'clients.id')
           ->select('tags.id','tags.name as tag_name','tags.description','clients.name')
           ->where('tags.enterprise_id', $enterprise_id); 
       } //lista apenas tags da empresa logada

        return $this->applyScopes($tags);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%'])
            ->ajax('')
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => false,
                'buttons' => [
                    'print',
                    'reset',
                    'reload',
                    [
                         'extend'  => 'collection',
                         'text'    => '<i class="fa fa-download"></i> Exportar',
                         'buttons' => [
                             'csv',
                             'excel',
                             'pdf',
                         ],
                    ],
                    'colvis'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'nome' => ['name' => 'name', 'data' => 'tag_name'],
            'descrição' => ['name' => 'description', 'data' => 'description'],
            'cliente' => ['name' => 'client', 'data' => 'name'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'tags';
    }
}
