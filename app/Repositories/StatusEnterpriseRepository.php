<?php

namespace App\Repositories;

use App\Models\StatusEnterprise;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class StatusEnterpriseRepository
 * @package App\Repositories
 * @version August 30, 2017, 5:06 pm UTC
 *
 * @method StatusEnterprise findWithoutFail($id, $columns = ['*'])
 * @method StatusEnterprise find($id, $columns = ['*'])
 * @method StatusEnterprise first($columns = ['*'])
*/
class StatusEnterpriseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StatusEnterprise::class;
    }
}
