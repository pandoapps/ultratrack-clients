<?php

namespace App\Repositories;

use App\Models\Listen;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ListenRepository
 * @package App\Repositories
 * @version September 20, 2017, 3:43 pm BRT
 *
 * @method Listen findWithoutFail($id, $columns = ['*'])
 * @method Listen find($id, $columns = ['*'])
 * @method Listen first($columns = ['*'])
*/
class ListenRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'speed',
        'temperature',
        'batery',
        'listened_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Listen::class;
    }
}
