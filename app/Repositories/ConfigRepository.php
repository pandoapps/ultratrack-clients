<?php

namespace App\Repositories;

use App\Models\Config;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ConfigRepository
 * @package App\Repositories
 * @version August 30, 2017, 5:29 pm UTC
 *
 * @method Config findWithoutFail($id, $columns = ['*'])
 * @method Config find($id, $columns = ['*'])
 * @method Config first($columns = ['*'])
*/
class ConfigRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'value'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Config::class;
    }
}
