<?php

namespace App\Repositories;

use App\Models\EventAction;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class EventActionRepository
 * @package App\Repositories
 * @version August 30, 2017, 5:28 pm UTC
 *
 * @method EventAction findWithoutFail($id, $columns = ['*'])
 * @method EventAction find($id, $columns = ['*'])
 * @method EventAction first($columns = ['*'])
*/
class EventActionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return EventAction::class;
    }
}
