<?php

namespace App\Repositories;

use App\Models\StatusTag;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class StatusTagRepository
 * @package App\Repositories
 * @version August 31, 2017, 1:09 pm UTC
 *
 * @method StatusTag findWithoutFail($id, $columns = ['*'])
 * @method StatusTag find($id, $columns = ['*'])
 * @method StatusTag first($columns = ['*'])
*/
class StatusTagRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StatusTag::class;
    }
}
