<?php

namespace App\Repositories;

use App\Models\Enterprise;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class EnterpriseRepository
 * @package App\Repositories
 * @version September 12, 2017, 12:08 pm UTC
 *
 * @method Enterprise findWithoutFail($id, $columns = ['*'])
 * @method Enterprise find($id, $columns = ['*'])
 * @method Enterprise first($columns = ['*'])
*/
class EnterpriseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'phone',
        'document',
        'logo',
        'field_key',
        'endpoint'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Enterprise::class;
    }
}
