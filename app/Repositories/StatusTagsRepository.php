<?php

namespace App\Repositories;

use App\Models\StatusTags;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class StatusTagsRepository
 * @package App\Repositories
 * @version August 30, 2017, 5:11 pm UTC
 *
 * @method StatusTags findWithoutFail($id, $columns = ['*'])
 * @method StatusTags find($id, $columns = ['*'])
 * @method StatusTags first($columns = ['*'])
*/
class StatusTagsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StatusTags::class;
    }
}
