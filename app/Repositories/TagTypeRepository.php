<?php

namespace App\Repositories;

use App\Models\TagType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TagTypeRepository
 * @package App\Repositories
 * @version August 30, 2017, 5:10 pm UTC
 *
 * @method TagType findWithoutFail($id, $columns = ['*'])
 * @method TagType find($id, $columns = ['*'])
 * @method TagType first($columns = ['*'])
*/
class TagTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TagType::class;
    }
}
