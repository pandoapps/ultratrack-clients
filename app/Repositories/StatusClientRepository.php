<?php

namespace App\Repositories;

use App\Models\StatusClient;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class StatusClientRepository
 * @package App\Repositories
 * @version August 30, 2017, 5:24 pm UTC
 *
 * @method StatusClient findWithoutFail($id, $columns = ['*'])
 * @method StatusClient find($id, $columns = ['*'])
 * @method StatusClient first($columns = ['*'])
*/
class StatusClientRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StatusClient::class;
    }
}
