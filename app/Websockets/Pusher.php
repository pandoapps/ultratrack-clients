<?php
namespace App\Websockets;

use Ratchet\ConnectionInterface;
use Ratchet\Wamp\WampServerInterface;
use Auth;

class Pusher implements WampServerInterface {
    
    /**
     * A lookup of all the topics clients have subscribed to
     */
    // Array com a lista dos assuntos (enterprise_id), a quem se
    //          deve disparar mensagens quando tal assunto recebe entrada de Listen
    protected $subscribedTopics = array();
    
    // Cadastra o Usuário com sessao aberta na página user_tags como interessado em 
    //          receber atualizações das tags e listens que ele tem acesso (segundo seu enterprise_id)
    public function onSubscribe(ConnectionInterface $conn, $topic) {
        $this->subscribedTopics[$topic->getId()] = $topic;
    }

    /**
     * @param string JSON'ified string we'll receive from ZeroMQ
     * Na entrada de uma Listen no banco (método ListenController@store), avisa todos que se inscreveram
     * naquele tópico **/
    public function onListenEntry($entry) {

        $entryData = json_decode($entry, true);

        //Se ninguém se inscreveu para receber este tópico
       if (!array_key_exists($entryData['tag']['id'], $this->subscribedTopics)) {
            return;
        }

       $topic = $this->subscribedTopics[$entryData['tag']['id']];

       // re-send the data to all the clients subscribed to that category
        $topic->broadcast($entryData);
    }

    /* The rest of our methods were as they were, omitted from docs to save space */

    public function onUnSubscribe(ConnectionInterface $conn, $topic) {
    }
    public function onOpen(ConnectionInterface $conn) {
    }
    public function onClose(ConnectionInterface $conn) {
    }
    public function onCall(ConnectionInterface $conn, $id, $topic, array $params) {
        // In this application if clients send data it's because the user hacked around in console
        $conn->callError($id, $topic, 'You are not allowed to make calls')->close();
    }
    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible) {
        // In this application if clients send data it's because the user hacked around in console
        $conn->close();
    }
    public function onError(ConnectionInterface $conn, \Exception $e) {
    }
}