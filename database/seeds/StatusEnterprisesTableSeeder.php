<?php

use Illuminate\Database\Seeder;

class StatusEnterprisesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = date("Y-m-d H:i:s");
        
        \App\Models\StatusEnterprise::insert([
            [
                "id"         	=>	1,
                "name"   		=>	'Ativo',
                "description"   =>	'Ativo',
                "created_at"    =>  $now,
                "updated_at"    =>  $now,
            ], 
            [
                "id"         	=>	2,
                "name"   		=>	'Inativo',
                "description"   =>	'Inativo',
                "created_at"    =>  $now,
                "updated_at"    =>  $now,
            ], 
        ]);
    }
}