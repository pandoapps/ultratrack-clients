<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ConfigsTableSeeder::class);
        $this->call(StatusEnterprisesTableSeeder::class);
        $this->call(StatusTagsTableSeeder::class);
        $this->call(TagTypesTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(StatusClientsTableSeeder::class);
        $this->call(EnterprisesTableSeeder::class);
        $this->call(ClientsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(GeneralSeeder::class);
        $this->call(ListensTableSeeder::class);
        $this->call(EventActionsTableSeeder::class);
    }
}
