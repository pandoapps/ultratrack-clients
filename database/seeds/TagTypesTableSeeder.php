<?php

use Illuminate\Database\Seeder;

class TagTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = date("Y-m-d H:i:s");
        
        \App\Models\TagType::insert([
            [
                "id"         	=>	1,
                "name"   		=>	'Modelo 001',
                "description"   =>	'Modelo 001',
                "picture"   	=>	'/public/images/Modelo 001.png',
                "created_at"    =>  $now,
                "updated_at"    =>  $now,
            ],
        ]);
    }
}