<?php

use Illuminate\Database\Seeder;
use App\Models\EventAction;

class EventActionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = date("Y-m-d H:i:s");

        EventAction::insert(
            [
                'last_point'        => 'SRID=4326;POINT(-22.42617768842507 -45.444045269775415)',
                'tag_id'            => 1,
                'listened_at'       => $now,
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );        
    }
}