<?php

use Illuminate\Database\Seeder;

class StatusTagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = date("Y-m-d H:i:s");
        
        \App\Models\StatusTag::insert([
            [
                "id"         	=>	1,
                "name"   		=>	'Ativo',
                "description"   =>	'Ativo',
                "created_at"    =>  $now,
                "updated_at"    =>  $now,
            ], 
            [
                "id"         	=>	2,
                "name"   		=>	'Inativo',
                "description"   =>	'Inativo',
                "created_at"    =>  $now,
                "updated_at"    =>  $now,
            ], 
            [
                "id"         	=>	3,
                "name"   		=>	'Com defeito',
                "description"   =>	'Com defeito',
                "created_at"    =>  $now,
                "updated_at"    =>  $now,
            ], 
        ]);
    }
}
