<?php

use Illuminate\Database\Seeder;
use App\Models\Listen;
use App\Models\Tag;

class ListensTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = date("Y-m-d H:i:s");

        //listens da tag de id 1

        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.414509572627765 -45.44229844557066)',
                'tag_id'            => 1,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 9*(24.2 * 60 * 60))), //9 dias anterior a data hoje
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.412424241307512 -45.45128552215118)',
                'tag_id'            => 1,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 9*(24.1 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.414509572627765 -45.44229844557066)',
                'tag_id'            => 1,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 9*(24 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.414509572627765 -45.44229844557066)',
                'tag_id'            => 1,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 8*(24 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.414509572627765 -45.44229844557066)',
                'tag_id'            => 1,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 7*(24.3 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.41276146704348 -45.44931141631622)',
                'tag_id'            => 1,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 7*(24.2 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.414844313740737 -45.4508563687088)',
                'tag_id'            => 1,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 7*(24.1 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.414509572627765 -45.44229844557066)',
                'tag_id'            => 1,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 7*(24 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.414509572627765 -45.44229844557066)',
                'tag_id'            => 1,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 6*(24.2 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.416312205985484 -45.45225111739654)',
                'tag_id'            => 1,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 6*(24.1 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.414509572627765 -45.44229844557066)',
                'tag_id'            => 1,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 6*(24 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.414509572627765 -45.44229844557066)',
                'tag_id'            => 1,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 5*(24 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
       Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.414509572627765 -45.44229844557066)',
                'tag_id'            => 1,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 4*(24 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.414509572627765 -45.44229844557066)',
                'tag_id'            => 1,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 3*(24 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.414509572627765 -45.44229844557066)',
                'tag_id'            => 1,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 2*(24 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.414509572627765 -45.44229844557066)',
                'tag_id'            => 1,
                'listened_at'       => date('d-m-Y H:i:s', time() - (26 * 60 * 60)),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.41837516345968 -45.45613495605011)',
                'tag_id'            => 1,
                'listened_at'       => date('d-m-Y H:i:s', time() - (25 * 60 * 60)),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.414509572627765 -45.44229844557066)',
                'tag_id'            => 1,
                'listened_at'       => date('d-m-Y H:i:s', time() - (24 * 60 * 60)),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.414509572627765 -45.44229844557066)',
                'tag_id'            => 1,
                'listened_at'       => $now,
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.419942197216162 -45.45353857772369)',
                'tag_id'            => 1,
                'listened_at'       => $now,
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.414509572627765 -45.44229844557066)',
                'tag_id'            => 1,
                'listened_at'       => $now,
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );

        $latlong = 'SRID=4326;POINT(-22.414509572627765 -45.44229844557066)';
        
        $tag = Tag::find(1);            
        $tag->last_point = $latlong;
        $tag->degrees = 5;
        $tag->save();
        
        //listens da tag de id2

        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.415578261802157 -45.44201580779571)',
                'tag_id'            => 2,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 9*(24.2 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.41722467172439 -45.443238895106504)',
                'tag_id'            => 2,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 9*(24.1 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.415578261802157 -45.44201580779571)',
                'tag_id'            => 2,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 9*(24 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.415578261802157 -45.44201580779571)',
                'tag_id'            => 2,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 8*(24 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.415578261802157 -45.44201580779571)',
                'tag_id'            => 2,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 7*(24.3 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.419317369420156 -45.447176377940366)',
                'tag_id'            => 2,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 7*(24.2 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.42207452487578 -45.44799176948089)',
                'tag_id'            => 2,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 7*(24.1 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.415578261802157 -45.44201580779571)',
                'tag_id'            => 2,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 7*(24 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.415578261802157 -45.44201580779571)',
                'tag_id'            => 2,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 6*(24.2 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.423909292234164 -45.449955146479795)',
                'tag_id'            => 2,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 6*(24.1 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.42208444260234 -45.45038429992218)',
                'tag_id'            => 2,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 6*(24 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.415578261802157 -45.44201580779571)',
                'tag_id'            => 2,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 5*(24 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
       Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.415578261802157 -45.44201580779571)',
                'tag_id'            => 2,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 4*(24 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.415578261802157 -45.44201580779571)',
                'tag_id'            => 2,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 3*(24 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.415578261802157 -45.44201580779571)',
                'tag_id'            => 2,
                'listened_at'       => date('d-m-Y H:i:s', (time() - 2*(24 * 60 * 60))),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.415578261802157 -45.44201580779571)',
                'tag_id'            => 2,
                'listened_at'       => date('d-m-Y H:i:s', time() - (26 * 60 * 60)),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.421033159644256 -45.44611422317047)',
                'tag_id'            => 2,
                'listened_at'       => date('d-m-Y H:i:s', time() - (25 * 60 * 60)),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.415578261802157 -45.44201580779571)',
                'tag_id'            => 2,
                'listened_at'       => date('d-m-Y H:i:s', time() - (24 * 60 * 60)),
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.415578261802157 -45.44201580779571)',
                'tag_id'            => 2,
                'listened_at'       => $now,
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );
        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => 'SRID=4326;POINT(-22.422659669531225 -45.443432014155576)',
                'tag_id'            => 2,
                'listened_at'       => $now,
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );

        $latlong = 'SRID=4326;POINT(-22.415578261802157 -45.44201580779571)';

        Listen::insert(
            [
                'speed'             => '5',
                'temperature'       => '5',
                'batery'            => '5',
                'last_point'        => $latlong,
                'tag_id'            => 2,
                'listened_at'       => $now,
                'updated_at'        => $now,
                'created_at'        => $now,
            ]
        );

        $tag = Tag::find(2);            
        $tag->last_point = $latlong;
        $tag->degrees = 5;
        $tag->save();
        
    }
}