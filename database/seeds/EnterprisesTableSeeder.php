<?php

use Illuminate\Database\Seeder;

class EnterprisesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $now = date("Y-m-d H:i:s");

        \App\Models\Enterprise::insert([
            [
                "id"         	=>	1,
                "name"   		=>	'ABC',
                "email"         =>	'abc@gmail.com',
                "phone"         =>	'3600-0000',
                "document"      =>	'896355667',
                "logo"          =>  '/public/Home_files/logo.jpg',
                "status_enterprise_id" =>  1,
                "field_key"     =>  '45644s45d68',
                "endpoint"      =>  '/endpoint',
                "created_at"    =>  $now,
                "updated_at"    =>  $now,
            ], 
        ]);
    }
}
