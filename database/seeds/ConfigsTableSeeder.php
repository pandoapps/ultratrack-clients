<?php

use Illuminate\Database\Seeder;

class ConfigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $now = date("Y-m-d H:i:s");

        \App\Models\Config::insert([
            [
                "id"         	=>	1,
                "name"   		=>	'Taxa de captura de latitude e longitude',
                "description"   =>	'Taxa de captura de latitude e longitude',
                "is_active"     =>  true,
                "value"		    =>	'0',
                "created_at"    =>  $now,
                "updated_at"    =>  $now,
            ], 
            [
                "id"         	=>	2,
                "name"   		=>	'Taxa de envio',
                "description"   =>	'Taxa de envio',
                "is_active"		=>	true,
                "value"         =>  '0',
                "created_at"    =>  $now,
                "updated_at"    =>  $now,
            ], 
            [
                "id"         	=>	3,
                "name"   		=>	'Enviar latitude e longitude',
                "description"   =>	'Enviar latitude e longitude',
                "is_active"		=>	false,
                "value"         =>  '0',
                "created_at"    =>  $now,
                "updated_at"    =>  $now,
            ], 
            [
                "id"         	=>	4,
                "name"   		=>	'Enviar altitude',
                "description"   =>	'Enviar altitude ',
                "is_active"		=>	false,
                "value"         =>  '0',
                "created_at"    =>  $now,
                "updated_at"    =>  $now,
            ], 
        ]);
    }
}
