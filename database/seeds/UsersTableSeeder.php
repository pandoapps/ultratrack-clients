<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = Role::where('name', '=', 'client')->first();
        
        $user1 = \App\Models\User::create([
            'name' => 'Karen Dantas',
            'password' => Hash::make('123456'),
            'email' => 'karend@yahoo.com',
            'enterprise_id' => 1,
            'client_id' => 1,
        ]);
        $user1->attachRole($client);        

        $user2 = \App\Models\User::create([
            'name' => 'Ronaldo Moraes',
            'password' => Hash::make('123456'),
            'email' => 'ronaldo@gmail.com',
            'enterprise_id' => 1,
            'client_id' => 2,
        ]);
        $user2->attachRole($client);

        $enterprise = Role::where('name', '=', 'enterprise')->first();

        $user3 = \App\Models\User::create([
            'name' => 'ABC',
            'password' => Hash::make('123456'),
            'email' => 'abc@gmail.com',
            'enterprise_id' => 1,
        ]);
        $user3->attachRole($enterprise);  
    }
}