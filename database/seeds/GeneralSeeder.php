<?php

use Illuminate\Database\Seeder;

class GeneralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = date("Y-m-d H:i:s");

        \App\Models\Ce::insert(
            [
                'enterprise_id' => 1,
                'config_id'     => 1,
                'value'         => 10,
            ]
        );
        \App\Models\Ce::insert(
            [
                'enterprise_id' => 1,
                'config_id'     => 2,
                'value'         => 20,
            ]
        );

        \App\Models\Ct::insert(
            [
                'tag_id'        => 1,
                'config_id'     => 1,
                'value'         => 53,
            ]
        );
        \App\Models\Ct::insert(
            [
                'tag_id'        => 1,
                'config_id'     => 2,
                'value'         => 7,
            ]
        );        
    }
}