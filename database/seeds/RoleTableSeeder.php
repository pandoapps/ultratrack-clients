<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new Role();
        $client->name = 'client';
        $client->display_name = 'Cliente'; // optional
        $client->description  = 'Cliente'; // optional
        $client->save();
        
        $enterprise = new Role();
        $enterprise->name = 'enterprise';
        $enterprise->display_name = 'Empresa'; // optional
        $enterprise->description  = 'Empresa'; // optional
        $enterprise->save();
    }
}
