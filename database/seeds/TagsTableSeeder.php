<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $now = date("Y-m-d H:i:s");

        \App\Models\Tag::insert([
            [
                "id"         	=>	1,
                "name"   		=>	'Tag1',
                "description"   =>	'tag1',
                "last_point"    =>  'SRID=4326;POINT(-22.419703 -45.44528)',
                "degrees"       =>  '28',
                "enterprise_id" =>	1,
                "client_id"     =>	1,
                "status_tag_id" =>  1,
                "tag_type_id" =>  1,
                "created_at"    =>  $now,
                "updated_at"    =>  $now,
            ], 
            [
                "id"         	=>	2,
                "name"   		=>	'Tag2',
                "description"   =>	'tag2',
                "last_point"    =>  'SRID=4326;POINT(-22.43010 -45.4571)',
                "degrees"       =>  '28',
                "enterprise_id" =>	1,
                "client_id"     =>	2,
                "status_tag_id" =>  1,
                "tag_type_id" =>  1,
                "created_at"    =>  $now,
                "updated_at"    =>  $now,
            ], 
        ]);
    }
}
