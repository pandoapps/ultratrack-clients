<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $now = date("Y-m-d H:i:s");

        \App\Models\Client::insert([
            [
                "id"         	=>	1,
                "name"   		=>	'Karen Dantas',
                "email"         =>	'karend@yahoo.com',
                "phone"         =>	'3500-0000',
                "document"      =>	'889955652',
                "picture"          =>  '/public/Home_files/foto1.jpg',
                "status_client_id" =>  1,
                "enterprise_id" =>  1,
                "created_at"    =>  $now,
                "updated_at"    =>  $now,
            ], 
            [
                "id"         	=>	2,
                "name"   		=>	'Ronaldo Moraes',
                "email"         =>	'ronaldo@gmail.com',
                "phone"         =>	'3900-0000',
                "document"      =>	'785632159',
                "picture"          =>  '/public/Home_files/foto2.jpg',
                "status_client_id" =>  1,
                "enterprise_id" =>  1,
                "created_at"    =>  $now,
                "updated_at"    =>  $now,
            ], 
        ]);
    }
}
