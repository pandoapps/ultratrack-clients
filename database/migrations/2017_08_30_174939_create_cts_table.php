<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCtsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cts', function (Blueprint $table) {
            $table->integer('tag_id')->unsigned();
            $table->integer('config_id')->unsigned();
            $table->string('value', 45);
            $table->foreign('tag_id')->references('id')->on('tags');
            $table->foreign('config_id')->references('id')->on('configs');
            $table->primary(['tag_id', 'config_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cts');
    }
}
