<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 150);
            $table->string('email', 45)->unique();
            $table->string('phone', 45)->nullable();
            $table->string('document', 45);
            $table->string('picture')->nullable();
            $table->integer('status_client_id')->unsigned();
            $table->integer('enterprise_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('status_client_id')->references('id')->on('status_clients');
            $table->foreign('enterprise_id')->references('id')->on('enterprises');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clients');
    }
}
