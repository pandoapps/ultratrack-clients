<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTagsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 45);
            $table->string('description');
            $table->point('last_point')->nullable();
            $table->string('degrees')->nullable();
            $table->integer('enterprise_id')->nullable()->unsigned();
            $table->integer('client_id')->nullable()->unsigned();
            $table->integer('status_tag_id')->unsigned();
            $table->integer('tag_type_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('enterprise_id')->references('id')->on('enterprises');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('status_tag_id')->references('id')->on('status_tags');
            $table->foreign('tag_type_id')->references('id')->on('tag_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tags');
    }
}
