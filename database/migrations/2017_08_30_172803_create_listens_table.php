<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateListensTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('speed');
            $table->string('temperature');
            $table->string('batery');
            $table->point('last_point');
            $table->integer('tag_id')->unsigned();
            $table->timestamp('listened_at');
            $table->timestamps();
            $table->foreign('tag_id')->references('id')->on('tags');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('listens');
    }
}
