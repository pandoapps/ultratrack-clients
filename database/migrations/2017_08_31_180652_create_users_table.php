<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 45);
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone')->nullable();
            $table->string('picture')->nullable();
            $table->integer('enterprise_id')->nullable()->unsigned();
            $table->integer('client_id')->nullable()->unsigned();
            $table->rememberToken('remember_token');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('enterprise_id')->references('id')->on('enterprises');
            $table->foreign('client_id')->references('id')->on('clients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
