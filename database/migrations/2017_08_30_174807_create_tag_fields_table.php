<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTagFieldsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tag_fields', function (Blueprint $table) {
            $table->integer('tag_id')->unsigned();
            $table->integer('field_id')->unsigned();
            $table->string('value', 200);
            $table->softDeletes();
            $table->foreign('tag_id')->references('id')->on('tags');
            $table->foreign('field_id')->references('id')->on('fields');
            $table->primary(['tag_id', 'field_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tag_fields');
    }
}
