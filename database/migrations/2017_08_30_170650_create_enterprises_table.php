<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEnterprisesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enterprises', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 45);
            $table->string('email', 45)->unique();
            $table->string('phone', 45);
            $table->string('document', 45);
            $table->string('logo', 255);
            $table->string('field_key', 255);
            $table->string('endpoint', 45);
            $table->integer('status_enterprise_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('status_enterprise_id')->references('id')->on('status_enterprises');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('enterprises');
    }
}
