<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ces', function (Blueprint $table) {
            $table->integer('enterprise_id')->unsigned();
            $table->integer('config_id')->unsigned();
            $table->string('value', 45);
            $table->foreign('enterprise_id')->references('id')->on('enterprises');
            $table->foreign('config_id')->references('id')->on('configs');
            $table->primary(['enterprise_id', 'config_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ces');
    }
}
